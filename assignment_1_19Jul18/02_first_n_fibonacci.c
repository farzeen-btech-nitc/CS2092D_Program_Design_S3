/**
 * 02. Fibonacci numbers.
 * Read a number n from user.
 * Print first n Fibonacii numbers.
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>

int main() {
    printf("FIBONACCI NUMBERS\n");
    printf("-----------------\n");

    long long input;
    printf("Enter n: ");
    errno = 0;
    if(!scanf("%lld", &input)) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }
    if(input <= 0) {
        printf("ERROR: n must be greater than zero.\n");
        return 1;
    }
    if(errno) {
        printf("ERROR: Number too large.\n");
        return 1;
    }

    printf("First %lld fibonacci numbers are: \n", input);
    if(input==1) {
        printf("0\n");
        return 0;
    } else if (input==2) {
        printf("0, 1\n");
        return 0;
    }
    printf("0, 1");

    unsigned long long int fib_n_minus_2 = 0;
    unsigned long long int fib_n_minus_1 = 1;
    unsigned long long int fib_n;
    for(unsigned long long i = 3; i<=input; i++) {
        if(fib_n_minus_1 > ULLONG_MAX - fib_n_minus_2) {
            printf(" ..\nERROR: Calculation overflow.\n");
            return 1;
        }
        fib_n = fib_n_minus_1 + fib_n_minus_2;
        printf(", %llu", fib_n);
        fib_n_minus_2 = fib_n_minus_1;
        fib_n_minus_1 = fib_n;
    }
    printf("\n");
    return 0;
}
