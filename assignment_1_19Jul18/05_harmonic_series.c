/**
 * 05. Harmonic Series
 * Read a number n from user.
 * Output the sum of first n terms of the harmonic series.
 * Harmonic series: 1+1/2+1/3+1/4+.....
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>

int main() {
    printf("HARMONIC SERIES\n");
    printf("---------------\n");

    long long n;
    printf("Enter n: ");
    errno = 0;
    if(!scanf("%lld", &n)) {
        printf("ERROR: Invalid input.");
        return 1;
    }
    if(n < 0) {
        printf("ERROR: n must be positive.\n");
        return 1;
    }
    if(errno) {
        printf("ERROR: Number too large.\n");
        return 1;
    }

    double  sum = 0;
    //for(long long i=1;i<=n; i++) {
    for(long long i=n;i>=1; i--) {
       sum += ((double) 1)/i;
    }
    printf("The sum is: %f\n", sum);
    return 0;
}
