/**
 * 03. Primes between.
 * Read two numbers m, n from user.
 * Print prime numbers between n, m (exclusive of both).
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>

int main() {
    printf("PRIMES BETWEEN\n");
    printf("--------------\n");

    long long n, m;

    printf("Enter m, n: ");
    errno = 0;
    if(!scanf("%lld, %lld", &m, &n)) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }
    if(m <= 0) {
        printf("ERROR: n must be greater than zero.\n");
        return 1;
    }
    if(!(n>m)) {
        printf("ERROR: n must be greater than m.\n");
        return 1;
    }
    if(errno) {
        printf("ERROR: Number too large.\n");
        return 1;
    }


    /* Note: 'Between' is ambigous. Is it inclusive or exclusive of the extremes? */
    /* Assuming exclusive. */
    int are_any_primes_found = 0;
    for(long long i = m+1; i<n; i++) {
        int is_i_prime = 1;
        for(long long j = 2; j<=i/2; j++) {
            if(i%j == 0) {
                is_i_prime = 0;
                break;
            }
        }
        if(is_i_prime) {
            if(!are_any_primes_found) {
                are_any_primes_found = 1;
                printf("The primes between %lld and %lld are:\n", m, n);
                printf("%lld", i);
            } else {
                printf(", %lld", i);
            }
        }
    }
    if(!are_any_primes_found) {
        printf("There are no primes between %lld and %lld.\n", m, n);
    } else {
        printf("\n");
    }

    return 0;
}
