/**
 * 06. Pascal's Triangle
 * Read a number n from user.
 * Output Pascal's triangle with n rows.
 * In Pascal's triangle, every number will be the sum of the
 * two numbers diagonally top-left and top-right to it.
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>

int main() {
    printf("PASCAL's TRIANGLE\n");
    printf("-----------------\n");

    long long input;
    printf("Enter n: ");
    errno = 0;
    if(!scanf("%lld", &input)) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }
    if(input < 0) {
        printf("ERROR: n must be positive.\n");
        return 1;
    }
    if(errno) {
        printf("ERROR: Number too large.\n");
        return 1;
    }

    const long long rows = input;
    const long long cols = input + (input - 1);
    unsigned long long triangle[rows][cols];
    for(long long r=0; r<rows; r++) {
        for(long long c=0; c<cols; c++){
            triangle[r][c] = 0;
        }
    };
    triangle[0][input-1] = 1;
    triangle[rows-1][0] = 1;
    triangle[rows-1][cols-1] = 1;
    for(long long r=1; r<rows; r++) {
        for(long long c=1; c<cols-1; c++) {
            const long long upperleft  = triangle[r-1][c-1];
            const long long upperright = triangle[r-1][c+1];
            if(ULLONG_MAX - upperleft < upperright) {
                printf("\n ... \n\nERROR: Calculation overflow.\n");
                return 1;
            }
            triangle[r][c] = upperleft + upperright;
        }
    }
    for(long long r=0; r<rows; r++) {
        for(long long c=0; c<cols; c++) {
            if(triangle[r][c] !=0) printf(" %3llu", triangle[r][c]);
            else printf("    ");
        }
        printf("\n");
    }
    return 0;
}
