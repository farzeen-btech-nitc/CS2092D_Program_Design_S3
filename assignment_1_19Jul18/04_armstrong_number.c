/**
 * 04. Armstrong Number
 * Read a number x from user.
 * Output whether x is an Armstrong number or not.
 * Ar Armstrong number is a number sum of cubes of whose digits equal that number.
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>

int main() {
    printf("ARMSTRONG NUMBER\n");
    printf("----------------\n");

    long long input;
    printf("Enter x: ");
    errno = 0;
    if(!scanf("%lld", &input)) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }
    if(input < 0) {
        printf("ERROR: x must be positive.\n");
        return 1;
    }
    if(errno) {
        printf("ERROR: Number too large.\n");
        return 1;
    }

    long long          input_copy   = input;
    unsigned long long sum_of_cubes = 0;
    unsigned long long ones_digit;
    while(input_copy>0) {
        ones_digit = input_copy % 10;
        sum_of_cubes += ones_digit*ones_digit*ones_digit;
        input_copy/=10;
    }
    if(sum_of_cubes == input) {
        printf("%lld is an Armstrong number.\n", input);
    } else {
        printf("%lld is not an Armstrong number.\n", input);
    }

    return 0;
}
