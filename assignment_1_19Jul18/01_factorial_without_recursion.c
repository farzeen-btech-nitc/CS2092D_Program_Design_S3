/**
 * 01. Factorial without recursion.
 * Read a number x from user.
 * Find the factorial of x without using recursion and print it.
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>

int main() {
    printf("FACTORIAL CALCULATOR\n");
    printf("--------------------\n");

    long long input;
    printf("Enter x: ");
    errno = 0;
    if(!scanf("%lld", &input)) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }
    if(input < 0) {
        printf("ERROR: x must be positive.\n");
        return 1;
    }
    if(errno) {
        printf("ERROR: Number too large.\n");
        return 1;
    }

    unsigned long long factorial = 1;
    for(long long i = 2; i <= input; i++) {
        if(factorial > ULLONG_MAX/i) {
            printf("ERROR: Calculation overflow.\n");
            return 1;
        }
        factorial *= i;
    }
    printf("x! is %llu\n", factorial);
    return 0;
}
