#!/bin/bash
exe=$1
test_file=$2
no_of_lines=$3

if [ -z "$exe" -o -z "$test_file" -o -z "$no_of_lines" ]; then
    echo
    echo "Usage:   ./auto_split_test_cases.bash    program_executable    test_file    no_of_lines"
    echo
    echo "program_executable : Name of the compiled executable"
    echo "test_file          : Name of the test file with lots of test cases"
    echo "no_of_lines        : No of lines in a single test case"
    echo
    exit
fi

if [ ! -f "$exe" ]; then
    echo "ERROR: Executable not found '$exe'"
    exit
fi

if [ ! -x "$exe" ]; then
    echo "ERROR: File found, but is not an executable '$exe'"
    exit
fi

if [ ! -f "$test_file" ]; then
    echo "ERROR: Test file not found '$test_file'"
    exit
fi

_is_integer=`echo "$no_of_lines" | grep -E ^\-?[0-9]+$`
if [ -z $_is_integer ]; then
    echo "ERROR: No of lines is not an integer '$no_of_lines'"
    exit
fi

if [ ! $no_of_lines -gt 0 ]; then
    echo "ERROR: No of lines must be greater than zero '$no_of_lines'"
    exit
fi

mv -n $test_file $test_file.bak
split -l $no_of_lines -d $test_file.bak __TEMP_SPLIT
for i in __TEMP_SPLIT*; do
    echo "Test case: ${i#__TEMP_SPLIT}"
    mv $i $test_file
    $exe
    rm $test_file
done
mv $test_file.bak $test_file
