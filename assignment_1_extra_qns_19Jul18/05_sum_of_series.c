#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>

int main(void)
{
    double x;
    long long num_terms;

    errno = 0;
    printf("Enter x: ");
    if(!scanf("%lf", &x)||errno) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }

    printf("Enter number of terms: ");
    if(!scanf("%lld", &num_terms)||errno) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }

    double x_pow = 0;
    double sum   = 0;

    printf("The sum of %lld terms = ... \r", num_terms);
    fflush(0);
    x_pow = x;
    sum += x_pow;
    for(long long i = 2; i<=num_terms; i++) {
        x_pow *= (-x)*x;
        sum += x_pow;
    }
    if(isnan(sum)) {
        printf("ERROR: Calculation overflow.     \n");
        return 1;
    }
    printf("The sum of %lld terms = %lf  \n", num_terms, sum);

    return 0;
}
