#include <errno.h>
#include <stdio.h>

int main(void)
{
    long long input;
    int freq[10];
    for(int i=0;i<9;i++)freq[i] = 0;
    printf("Enter the number: ");
    errno = 0;
    if(!scanf("%lld", &input)||errno) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }
    while(input>0) {
        int digit = input % 10;
        freq[digit]++;
        input/=10;
    }
    printf("The frequency of digits are: \n");
    for(int i=0; i<9; i++) {
        if(freq[i]==0) continue;
        printf("  %d: %d\n", i, freq[i]);
    }
    return 0;
}
