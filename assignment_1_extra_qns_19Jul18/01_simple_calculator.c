#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

void exit_invalid_input();
void exit_invalid_operation(char op);
void exit_calculation_overflow();

double op_add(double op1, double op2);
double op_sub(double op1, double op2);
double op_mul(double op1, double op2);
double op_div(double op1, double op2);

int main() {
    printf("SIMPLE CALCULATOR\n");
    printf("-----------------\n");

    printf("Enter a binary expression:\n");
    printf("(Supported operations: +, -, *, /) \n");

    double operand1;
    double operand2;
    char operation;
    double result;

    errno = 0;
    if(!scanf("%lf ", &operand1)||errno)  exit_invalid_input();
    if(!scanf("%c", &operation)||errno)   exit_invalid_input();
    if(!scanf("%lf", &operand2)||errno)  exit_invalid_input();

    switch(operation) {
        case '+':   result = op_add(operand1, operand2); break;
        case '-':   result = op_sub(operand1, operand2); break;
        case '*':   result = op_mul(operand1, operand2); break;
        case '/':   result = op_div(operand1, operand2); break;
        default:    exit_invalid_operation(operation);
    }

    printf(" = %.3lf\n", result);


    return 0;
}

void exit_invalid_input() {
    printf("ERROR: Invalid input.\n");
    exit(1);
}

void exit_invalid_operation(char op) {
    printf("ERROR: Invalid operation: '%c'.\n", op);
    exit(1);
}

void exit_calculation_overflow() {
    printf("\n  ...\nERROR: Calculation overflow.\n");
    exit(1);
}

double op_add(double op1, double op2) {
    return op1+op2;
}
double op_sub(double op1, double op2) {
    return op1-op2;
}
double op_mul(double op1, double op2) {
    return op1*op2;
}
double op_div(double op1, double op2) {
    return op1/op2;
}
