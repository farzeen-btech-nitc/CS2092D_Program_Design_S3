#include <errno.h>
#include <stdio.h>

int main() {

    long long n1, n2;
    long long gcd = 1;
    errno=0;
    printf("Enter n1: ");
    if(!scanf("%lld", &n1)||errno||n1<0) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }

    printf("Enter n2: ");
    if(!scanf("%lld", &n2)||errno||n2<0) {
        printf("ERROR: Invalid input.\n");
        return 1;
    }

    printf("GCD(%lld, %lld) = ...\r", n1, n2);
    fflush(0);

    long long min = n1<n2?n1:n2;
    for(long long i = 2; i<=min; i++) {
        if ((n1 % i) == 0 && (n2 % i) == 0) gcd = i;
    }

    printf("GCD(%lld, %lld) = %lld    \n", n1, n2, gcd);

}

