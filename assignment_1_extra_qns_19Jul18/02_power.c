#include <errno.h>
#include <limits.h>
#include <stdio.h>

int main() {

    printf("POWER FUNCTION\n");
    printf("--------------\n");

    long long base;
    long long exponent;

    printf("Enter the base: ");
    if(!scanf("%lld", &base)||errno) {
        printf("ERROR: Invalid input.\n"); return 1;
    }
    printf("Enter the exponent: ");
    if(!scanf("%lld", &exponent)||errno) {
        printf("ERROR: Invalid input.\n"); return 1;
    }

    printf(" = .. \r");
    fflush(0);

    if(exponent>=0) {
        long long result = 1;
        for(; exponent>0; exponent --) {
             if(result> LLONG_MAX/base) {
                 printf("Calculation overflow.\n");
                 return 1;
             }
             result*=base;
        }
        printf(" = %lld \n", result);
    } else {
        double result = 1;
        for(; exponent<0; exponent ++) {
             result/=base;
        }
        printf(" = %lf \n", result);
    }
    return 0;
}

