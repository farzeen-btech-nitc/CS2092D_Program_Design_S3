#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

struct LinkedList;
typedef struct LinkedList *Node;

struct LinkedList {
    int coeff;
    int degree;
    Node next;
};

Node ll_create_node() {
    Node node = malloc(sizeof(struct LinkedList));
    node->next = NULL;
    return node;
}

void ll_append_node(Node node, int coeff, int degree) {
    node->next = ll_create_node();
    node->coeff = coeff;
    node->degree = degree;
}

const char *unicode_sup = "⁰¹²³⁴⁵⁶⁷⁸⁹";

Node read_polynomial_as_ll(const char* name) {
    printf("Enter the terms of the %s polynomial (ceoff, degree):\n", name);

    int coeff, degree;

    Node head = ll_create_node();
    read_int(&coeff);
    read_uint(&degree);

    head->coeff = coeff;
    head->degree = degree;

    Node current = head;
    while(degree>0) {
        read_int(&coeff);
        read_uint(&degree);
        ll_append_node(current, coeff, degree);
        current = current->next;
    }

    Node head_old = head;
    head = head->next;
    free(head_old);
    return head;
}

int main() {

    Node head_1 = read_polynomial_as_ll("first");
    Node head_2 = read_polynomial_as_ll("second");

    Node head_sum = ll_create_node();

    Node smaller_poly_head, larger_poly_head;
    if (head_1->degree > head_2->degree) {
        larger_poly_head = head_1;
        smaller_poly_head = head_2;
    } else {
        larger_poly_head = head_2;
        smaller_poly_head = head_1;
    }

    Node current_1 = head_1;
    Node current_2 = head_2;

    int degree = larger_poly_head->degree;
    while(1) {






    return 0;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
