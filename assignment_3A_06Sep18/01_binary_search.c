#include<stdio.h>
#include<stdlib.h>


long __binary_search(long *array, long start, long end, long key) {
    long mid = (start+end)/2;
    if(start>end) return -1;
    if(array[mid] == key) {
        while(1) {
            if(mid==0) break;
            if(array[mid-1] == key) mid -= 1;
            else break;
        }
        return mid;
    }
    if(key < array[mid]) return __binary_search(array, start, mid-1, key);
    if(key > array[mid]) return __binary_search(array, mid+1, end,   key);
}
long binary_search(long *array, long len, long key) {
    return __binary_search(array, 0, len-1, key);
}

int main() {
    FILE *fp = fopen("1.txt", "r");

    long N;
    long *array;
    long key;

    fscanf(fp, "%ld", &N);
    if(N<0) {
        printf("ERROR: Invalid number of elements.\n");
        return 1;
    }
    array = malloc(N * sizeof(long));

    for(int i = 0; i<N; i++) {
        if(fscanf(fp, "%ld", &array[i]) != 1) {
            printf("ERROR: \n");
            printf("Sufficient number of array elements not recieved.\n");
            free(array);
            return 1;
        }
    }

    if(fscanf(fp, "%ld", &key) != 1) {
        printf("ERROR: \n");
        printf("Key not received.\n");
        free(array);
        return 1;
    }

    printf("The number of elements in the array is\t: %ld\n", N);
    printf("The array elements are\t\t\t: ");
    for(int i=0; i<N; i++) {
        printf("%ld ", array[i]);
    }
    printf("\n");

    for(int i=1;i<N; i++) {
        if(array[i-1]>array[i]) {
            printf("ERROR: Array not sorted\n");
            free(array);
            return 1;
        }
    }


    long search_result = binary_search(array, N, key);
    if(search_result>=0) {
        printf("The key found in the position\t\t: %ld\n", search_result);
    } else {
        printf("\t\t\t\t\t  NOT FOUND\n");
    }

    free(array);

}
