#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void print_array(long *array, long len) {
    for(int i=0; i<len; i++) {
        printf("%d ", array[i]);
    }
    printf("\b\n");
}

void insertion_sort(long *array, long len) {
    for(long i = 0;i<len; i++) {
        // Invariant: array[0..i-1] is sorted in ascending order.
        //            No new elements are added to array[0..i-1] that are not in
        //            the original.

        // Insert array[i] into the subarray array[0..i-1]
        // so that array[0..i] is sorted.
        long j = i-1;
        long key = array[i];
        while(j>=0 && array[j] > key) {
            // Invariant: all elements of array[j+1..i]
            //            are greater than or equal to key
            array[j+1] = array[j];
            array[j] = key;
            // Invariant': all elements of array[j..i]
            //             are greater than or equal to key
            j --;
        }

        // Invariant(i+1): All elements of array[0..i] is sorted in ascending order.
    }
}

int main() {
    FILE *fp = fopen("5.txt", "r");

    long N;
    long *array;

    fscanf(fp, "%ld", &N);
    array = malloc(N * sizeof(long));

    for(int i = 0; i<N; i++) {
        fscanf(fp, "%ld", &array[i]);
    }

    insertion_sort(array, N);
    printf("The sorted list is\t: ");
    print_array(array, N);

    free(array);

}
