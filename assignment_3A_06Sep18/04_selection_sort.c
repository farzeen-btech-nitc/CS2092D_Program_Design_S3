#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void print_array(long *array, long len) {
    for(int i=0; i<len; i++) {
        printf("%ld ", array[i]);
    }
    printf("\b\n");
}

void selection_sort(long *array, long len, int shall_print) {
    for(long i = 0;i<len-1; i++) {
        long min_i = i;
        for(long j=i+1; j<len; j++) {
            if(array[j]<array[min_i]) min_i = j;
        }
        long temp = array[i];
        array[i] = array[min_i];
        array[min_i] = temp;
        if(shall_print) {
            printf("Iteration %ld\t\t: ", i+1);
            print_array(array, len);
        }
    }
}

int main() {
    FILE *fp = fopen("4.txt", "r");

    long N;
    long *array, *array_copy;

    if(fscanf(fp, "%ld", &N) != 1) {
        printf("ERROR: Unable to read N.\n");
        return 1;
    }
    if(N<0) {
        printf("ERROR: Invalid number of elements.\n");
        return 1;
    }
    array = malloc(N * sizeof(long));

    for(int i = 0; i<N; i++) {
        if(fscanf(fp, "%ld", &array[i]) != 1) {
            printf("ERROR: Insufficient number of elements.\n");
            free(array);
            return 1;
        }
    }

    memcpy(array_copy, array, N * sizeof(long));
    selection_sort(array, N, 0);
    printf("The sorted list is\t: ");
    print_array(array, N);
    selection_sort(array_copy, N, 1);
    free(array);

}
