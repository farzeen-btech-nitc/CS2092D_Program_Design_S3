#include<stdio.h>
#include<stdlib.h>


long linear_search(long *array, long len, long key) {
    for(long i = 0;i<len; i++) {
        if(array[i] == key) return i;
    }
    return -1;
}

int main() {
    FILE *fp = fopen("2.txt", "r");

    long key;
    long N;
    long *array;

    if(fscanf(fp, "%ld", &key) != 1) {
        printf("ERROR: Unable to read key.\n");
        return 1;
    }
    if(fscanf(fp, "%ld", &N) != 1) {
        printf("ERROR: Unable to read N.\n");
        return 1;
    }
    array = malloc(N * sizeof(long));

    for(int i = 0; i<N; i++) {
        if(fscanf(fp, "%ld", &array[i]) != 1) {
            printf("ERROR: Insufficient number of array elements.\n");
            free(array);
            return 1;
        }
    }

    long search_result = linear_search(array, N, key);
    if(search_result>=0) {
        printf("%d\n", search_result);
    } else {
        printf("NOT FOUND\n");
    }

    free(array);

}
