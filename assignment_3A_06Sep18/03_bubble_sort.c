#include<stdio.h>
#include<stdlib.h>


void bubble_sort(long *array, long len) {
    for(long i = 0;i<len; i++) {
        for(long j=0;j<len-i-1; j++) {
            if(array[j]>array[j+1]) {
                long temp  = array[j];
                array[j]   = array[j+1];
                array[j+1] = temp;
            }
        }
    }
}

int main() {
    FILE *fp = fopen("3.txt", "r");

    long N;
    long *array;

    if(fscanf(fp, "%ld", &N) != 1) {
        printf("ERROR: Unable to read array size.\n");
        return 1;
    }
    if(N<0) {
        printf("ERROR: Invalid array size.\n");
        return 1;
    }
    if(N==0) return 0;
    array = malloc(N * sizeof(long));

    for(int i = 0; i<N; i++) {
        if(fscanf(fp, "%ld", &array[i]) != 1) {
            printf("ERROR: Insufficient array elements.\n");
            free(array);
            return 1;
        }
    }

    bubble_sort(array, N);
    for(int i=0; i<N; i++) {
        printf("%ld ", array[i]);
    }
    printf("\b\n");
    free(array);
}
