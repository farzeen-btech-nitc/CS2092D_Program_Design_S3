#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Cyclically permute array.
 * Input:  A list of integers.
 * Output: The next cyclic permutation of given list.
 * Cyclic permutation: Pop one off the top, push it to the end.
 */

void read_int(int *var);
void read_uint(int *var);

int main() {
    int len;
    printf("Size of the array: ");
    read_uint(&len);
    int A[len];
    printf("Array elements:\n");
    for(int i = 0; i<len; i++) {
        read_int(&A[i]);
    }

    int A_permuted[len];
    for(int i = 0; i<len-1; i++) {
        A_permuted[i] = A[i+1];
    }
    A_permuted[len-1] = A[0];

    printf("Cyclically Permuted array is:\n");
    for(int i = 0; i<len-1; i++) {
        printf("%3d, ", A_permuted[i]);
    }
    printf("%3d\n", A_permuted[len-1]);
    return 0;
}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}

void read_uint(int *var) {
    read_int(var);
    if(*var<=0) {
        printf("ERROR: Input must be greater than zero.\n");
        exit(1);
    }
}

