#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Find and replace in array.
 * Input:  A list of integers, and two integers x, y.
 * Output: Input list after replacing all instances of x with y.
 */

void read_int(int *var);

int main() {
    printf("Size of the array: ");
    int num_elements;
    read_int(&num_elements);
    if(num_elements <= 0) {
        printf("ERROR: Number of elements must be greater than zero.\n");
        return 1;
    }

    int elements[num_elements];

    printf("Array elements: \n");
    for(int i = 0; i<num_elements; i++) {
        read_int(&elements[i]);
    }

    int x;
    printf("Enter the element to be replaced: ");
    read_int(&x);
    int y;
    printf("Enter the element to be inserted: ");
    read_int(&y);

    int is_x_found = 0;
    for(int i = 0;i<num_elements; i++) {
        if(elements[i] == x) {
            is_x_found = 1;
            elements[i] = y;
        }
    }
    if(!is_x_found) {
        printf("NOT FOUND\n");
        return 1;
    }
    printf("Resultant array is:\n");

    for(int i=0; i<num_elements-1; i++) {
        printf("%d, ", elements[i]);
    }
    printf("%d \n", elements[num_elements-1]);

    return 0;

}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}
