#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Largest Odd Integer.
 * Input:  A list of integers.
 * Output: The largest odd integer in the list.
 */

void read_int(int *var);

int main() {
    printf("Size of the array: ");
    int num_elements;
    read_int(&num_elements);
    if(num_elements <= 0) {
        printf("ERROR: Number of elements must be greater than zero.\n");
        return 1;
    }

    int elements[num_elements];

    printf("Array elements: \n");
    for(int i = 0; i<num_elements; i++) {
        read_int(&elements[i]);
    }

    int largest_odd;
    int any_odd_numbers_found = 0;
    for(int i = 0; i<num_elements; i++) {
        if(elements[i] % 2 == 1) {
            if(any_odd_numbers_found) {
                if(elements[i]>largest_odd) largest_odd = elements[i];
            } else {
                largest_odd = elements[i];
                any_odd_numbers_found = 1;
            }
        }
    }

    if(any_odd_numbers_found) printf("Largest odd integer is: %d\n", largest_odd);
    else printf("No odd numbers were given.\n");

    return 0;
}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}
