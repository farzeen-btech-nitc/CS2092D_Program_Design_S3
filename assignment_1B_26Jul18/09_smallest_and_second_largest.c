#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Smallest and second largest.
 * Input:  A list of integers.
 * Output: Smallest element in the list, and second largest element in the list.
 */

void read_int(int *var);
void read_uint(int *var);

int main() {
    int len;
    printf("Size of the array: ");
    read_uint(&len);
    int A[len];
    printf("Array elements:\n");
    for(int i = 0; i<len; i++) {
        read_int(&A[i]);
    }

    int smallest = A[0], largest = A[0], second_largest = A[0];
    for(int i = 0; i<len; i++) {
        if(A[i]<smallest) smallest = A[i];
        if(A[i]>largest) {
            second_largest = largest;
            largest = A[i];
        } else if(largest == second_largest && A[i] < largest) second_largest = A[i];
          else if(A[i]>second_largest && A[i]<largest) second_largest = A[i];
    }

    printf("Second largest element in the array is: %d\n", second_largest);
    printf("Smallest element in the array is: %d\n", smallest);


    return 0;
}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}

void read_uint(int *var) {
    read_int(var);
    if(*var<=0) {
        printf("ERROR: Input must be greater than zero.\n");
        exit(1);
    }
}

