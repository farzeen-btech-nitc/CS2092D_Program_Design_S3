#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Print and delete ith element.
 * Input:  A list of integers, an index.
 * Output: A list of integers, excluding the element at that index.
 */

void read_int(int *var);

int main() {
    printf("Size of the array(n): ");
    int num_elements;
    read_int(&num_elements);
    if(num_elements <= 0) {
        printf("ERROR: Number of elements must be greater than zero.\n");
        return 1;
    }

    int elements[num_elements];

    printf("Enter the elements: \n");
    for(int i = 0; i<num_elements; i++) {
        read_int(&elements[i]);
    }

    printf("Position(k): ");
    int k;
    read_int(&k);
    if(k<0 || k>= num_elements) {
        printf("INVALID INDEX\n");
        return 1;
    }

    for(int i=k; i<num_elements-1; i++) {
        elements[i] = elements[i+1];
    }


    printf("Resultant array:\n");
    for(int i=0; i<num_elements-2; i++) {
        printf("%d, ", elements[i]);
    }
    printf("%d\n", elements[num_elements-2]);

    return 0;
}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}
