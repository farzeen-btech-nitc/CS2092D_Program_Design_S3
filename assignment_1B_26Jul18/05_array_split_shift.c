#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Split an array at given index and swap the two parts.
 * Input:  A list of integers, the index to split at.
 * Output: A list of integers, formed by splitting the input array into two at given index and swapping.
 */

void read_int(int *var);

int main() {
    printf("Size of the array: ");
    int num_elements;
    read_int(&num_elements);
    if(num_elements <= 0) {
        printf("ERROR: Number of elements must be greater than zero.\n");
        return 1;
    }

    int elements[num_elements];

    printf("Array elements: \n");
    for(int i = 0; i<num_elements; i++) {
        read_int(&elements[i]);
    }

    int k;
    printf("Enter the position of the element to split the array: ");
    read_int(&k);
    if(k<0||k>=num_elements) {
        printf("INVALID INDEX\n");
        return 1;
    }

    k = k-1; //because the Qn makers decided to count arrays from 1.

    int elements_new[num_elements];
    for(int i=k; i<num_elements; i++) {
        elements_new[i-k] = elements[i];
    }
    for(int i=0; i<k; i++) {
        elements_new[num_elements-k+i] = elements[i];
    }
    printf("Resultant array is:\n");
    for(int i=0; i<num_elements-1; i++) {
        printf("%d, ", elements_new[i]);
    }
    printf("%d \n", elements_new[num_elements-1]);

    return 0;

}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}
