#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Matrix transpose.
 * Input:  Integers m,n, and an m x n matrix of integers.
 * Output: Transpose of input matrix.
 */

void read_int(int *var);
void read_uint(int *var);

int main() {
    int m, n;
    printf("Enter the value of m: ");
    read_uint(&m);
    printf("Enter the value of n: ");
    read_uint(&n);

    int A[m][n];

    printf("Elements of the matrix: \n");
    for(int i = 0; i<m*n; i++) {
        read_int(&((int*) A)[i]);
    }

    int A_T[n][m];
    for(int r = 0; r<m; r++) {
        for(int c=0; c<n; c++) {
            A_T[c][r] = A[r][c];
        }
    }
    printf("\n");
    for(int r = 0; r<n; r++) {
        for(int c=0; c<m-1; c++) {
            printf("%3d, ", A_T[r][c]);
        }
        printf("%3d\n", A_T[r][m-1]);
    }
    printf("\n");

    return 0;

}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}

void read_uint(int *var) {
    read_int(var);
    if(*var<=0) {
        printf("ERROR: Input must be greater than zero.\n");
        exit(1);
    }
}
