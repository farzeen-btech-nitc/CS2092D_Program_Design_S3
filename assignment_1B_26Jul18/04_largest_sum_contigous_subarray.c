#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Contigous sub array with largest sum.
 * Input:  A list of integers.
 * Output: The largest odd integer in the list.
 */

void read_int(int *var);

int main() {
    printf("Size of the array: ");
    int num_elements;
    read_int(&num_elements);
    if(num_elements <= 0) {
        printf("ERROR: Number of elements must be greater than zero.\n");
        return 1;
    }

    int elements[num_elements];

    printf("Array elements: \n");
    for(int i = 0; i<num_elements; i++) {
        read_int(&elements[i]);
    }

    int max_begin=0, max_end=1, max_sum=elements[0]+elements[1];
    for(int begin = 0; begin<num_elements-1; begin++) {
        for(int end = begin+1; end<num_elements; end++) {
            int sum = 0;
            for(int i = begin; i<=end; i++) sum += elements[i];
            if(sum>max_sum) {
                max_sum = sum;
                max_begin = begin;
                max_end = end;
            }
        }
    }

    if(max_sum < 0) {
        printf("The largest sum is 0 and the subset is phi (null set).\n");
    } else {
        printf("The largest sum is %d and the subset index begins at %d and ends at %d.\n",
                                                                    max_sum, max_begin, max_end);
    }

    return 0;
}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}
