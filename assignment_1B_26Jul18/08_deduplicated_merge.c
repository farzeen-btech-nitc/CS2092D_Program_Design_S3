#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Deduplicated merge.
 * Input:  Two lists of integers.
 * Output: A list of integers which contain all the elements of input lists, with no elements repeated.
 */

void read_int(int *var);
void read_uint(int *var);
int  is_element_in_array(int elem, int *array, int len);

int main() {
    int len1;
    printf("Size of the first array: ");
    read_uint(&len1);
    int A1[len1];
    printf("Array elements:\n");
    for(int i = 0; i<len1; i++) {
        read_int(&A1[i]);
    }


    int len2;
    printf("Size of the second array: ");
    read_uint(&len2);
    int A2[len2];
    printf("Array elements:\n");
    for(int i = 0; i<len2; i++) {
        read_int(&A2[i]);
    }

    int A_merged[len1+len2];
    int len_merged=0;
    for(int i=0;i<len1; i++) {
        if(!is_element_in_array(A1[i], A_merged, len_merged)) {
            A_merged[len_merged] = A1[i];
            len_merged+=1;
        }
    }
    for(int i=0;i<len2; i++) {
        if(!is_element_in_array(A2[i], A_merged, len_merged)) {
            A_merged[len_merged] = A2[i];
            len_merged+=1;
        }
    }

    printf("Merged array:\n");
    for(int i=0; i<len_merged-1; i++) {
        printf("%3d, ", A_merged[i]);
    }
    if(len_merged>1) printf("%3d\n", A_merged[len_merged-1]);
    return 0;
}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}

void read_uint(int *var) {
    read_int(var);
    if(*var<=0) {
        printf("ERROR: Input must be greater than zero.\n");
        exit(1);
    }
}

int is_element_in_array(int elem, int* array, int len) {
    for(int i = 0;i<len; i++) {
        if(array[i] == elem) return 1;
    }
    return 0;
}
