#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Frequency of integers.
 * Input:  A list of integers.
 * Output: A list of integers, along with their frequency in the input list.
 */

void read_int(int *var);
void sort(int *array, int len);

int main() {
    printf("Enter the number of elements: ");
    int num_elements;
    read_int(&num_elements);
    if(num_elements <= 0) {
        printf("ERROR: Number of elements must be greater than zero.\n");
        return 1;
    }

    int elements[num_elements];

    printf("Enter the elements: \n");
    for(int i = 0; i<num_elements; i++) {
        printf("\t[%d]: ", i);
        read_int(&elements[i]);
    }

    sort(elements, num_elements);

    printf("Frequencies: \n");
    int last_element = elements[0];
    int last_element_freq = 1;
    for(int i = 1; i<num_elements; i++) {
        if(elements[i] == last_element) {
            last_element_freq += 1;
        } else {
            printf("\t%d\t: %d\n", last_element, last_element_freq);
            last_element = elements[i];
            last_element_freq = 1;
        }
    }
    printf("\t%d\t: %d\n", last_element, last_element_freq);

    return 0;
}

void read_int(int *var) {
    errno = 0;
    if(!scanf("%d,", var)) {
        printf("ERROR: Invalid integer.\n");
        exit(1);
    }
    if(errno) {
        printf("ERROR: Integer overflow.\n");
        exit(1);
    }
}

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sort(int *array, int len) {
    // bubble sort
    for(int i=1; i<len; i++) {
        for(int j=0; j<(len-i); j++) {
            if(array[j] > array[j+1]) {
                swap(&array[j], &array[j+1]);
            } //invariant: array[j+1] greater than all elements before it
        } //invariant on exit: array[len-i:] is sorted
    }
}


