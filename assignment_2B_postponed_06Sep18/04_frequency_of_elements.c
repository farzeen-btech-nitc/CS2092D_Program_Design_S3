#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

typedef struct LinkedList *Node;
struct LinkedList {
    int data;
    int freq;
    Node next;
};

Node ll_create_node() {
    Node node = malloc(sizeof(struct LinkedList));
    node->next = NULL;
    return node;
}

void ll_append_node(Node node, int data, int freq) {
    node->next = ll_create_node();
    node->next->data = data;
    node->next->freq = freq;
}

void ll_remove_next(Node node) {
    Node next_next = node->next->next;
    free(node->next);
    node->next = next_next;
}

int main() {

    Node head = ll_create_node();
    int N;
    printf("Enter the value of n\t: ");
    read_uint(&N);
    printf("Enter the elements of the linked list\t: ");
    int data;
    Node current = head;
    for(int i=0; i<N; i++) {
        read_uint(&data);
        ll_append_node(current, data, 1);
        current = current->next;
    }

    Node i = head->next;
    while(i!=NULL) {
        Node j = i->next;
        Node j_minus_1 = i;
        while(j != NULL) {
            if (j->data==i->data) {
                i->freq+=1;
                j = j->next;
                ll_remove_next(j_minus_1);
            } else {
                j_minus_1 = j;
                j = j->next;
            }
        }
        i = i->next;
    }

    i = head->next;
    while(i!=NULL) {
        printf("Number of occurrences of %d \t %d\n", i->data, i->freq);
        i = i->next;
    }
    return 0;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
