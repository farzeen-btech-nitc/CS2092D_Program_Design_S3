#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

typedef struct LinkedList *Node;
struct LinkedList {
    int data;
    Node next;
};

Node ll_create_node() {
    Node node = malloc(sizeof(struct LinkedList));
    node->next = NULL;
    return node;
}

void ll_append_node(Node node, int data) {
    node->next = ll_create_node();
    node->next->data = data;
}

Node ll_reverse(Node head) {
    Node previous = NULL;
    Node current = head;
    while(current != NULL) {
        Node next = current->next;

        current->next = previous;

        previous = current;
        current = next;
    }
    return previous;
}

int main() {

    Node head = ll_create_node();
    int N;
    printf("Enter list size\t\t: ");
    read_uint(&N);
    printf("Enter the elements of the linked list\t: ");
    int data;
    Node current = head;
    for(int i=0; i<N; i++) {
        read_uint(&data);
        ll_append_node(current, data);
        current = current->next;
    }
    Node head_old = head;
    head = head->next;
    free(head_old);

    head = ll_reverse(head);

    current = head;
    printf("Reversed list\t\t: ");
    while(current != NULL) {
        printf("%d, ", current->data);
        current = current->next;
    }
    printf("\b\b \n");

    return 0;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
