#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

typedef struct LinkedList *Node;
struct LinkedList {
    int data;
    Node next;
};

Node ll_create_node() {
    Node node = malloc(sizeof(struct LinkedList));
    node->next = NULL;
    return node;
}

void ll_append_node(Node node, int data) {
    node->next = ll_create_node();
    node->next->data = data;
}

Node read_sorted_linked_list(const char *name, int len) {
    printf("Enter the elements of linked list %s\t: ", name);
    Node head = ll_create_node();
    int data;
    Node current = head;
    for(int i=0; i<len; i++) {
        read_uint(&data);
        if(data < current->data) {
            printf("Error: Unsorted list.\n");
            exit(1);
        }
        ll_append_node(current, data);
        current = current->next;
    }
    Node head_old = head;
    head = head->next;
    free(head_old);
    return head;
}

Node merge_sorted_linked_list(Node head_A, Node head_B) {
    Node head_merged = ll_create_node();
    Node current = head_merged;
    while(1) {
        if(head_A != NULL && head_B != NULL) {
            if(head_A->data < head_B->data) {
                ll_append_node(current, head_A->data);
                current = current->next;
                head_A = head_A->next;
            } else {
                ll_append_node(current, head_B->data);
                current = current->next;
                head_B = head_B->next;
            }
        } else if(head_A != NULL) {
                ll_append_node(current, head_A->data);
                current = current->next;
                head_A = head_A->next;
        } else if(head_B != NULL) {
                ll_append_node(current, head_B->data);
                current = current->next;
                head_B = head_B->next;
        } else {
            break;
        }
    }
    Node head_old = head_merged;
    head_merged = head_merged->next;
    free(head_old);
    return head_merged;
}

int main() {

    int M,N;
    printf("Enter the value of M\t\t: ");
    read_uint(&M);
    printf("Enter the value of N\t\t: ");
    read_uint(&N);

    Node head_A = read_sorted_linked_list("A", M);
    Node head_B = read_sorted_linked_list("B", N);
    Node merged = merge_sorted_linked_list(head_A, head_B);


    Node current = merged;
    printf("Merged Linked List\t\t: ");
    while(current != NULL) {
        printf("%d, ", current->data);
        current = current->next;
    }
    printf("\b\b \n");

    return 0;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
