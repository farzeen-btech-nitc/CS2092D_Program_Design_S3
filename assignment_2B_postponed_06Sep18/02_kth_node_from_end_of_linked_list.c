#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Find kth node from the end of a singly linked list of
 * integers
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

/* Linked list functions ----------------------- */
struct LinkedList {
    int data;
    struct LinkedList *next;
};
typedef struct LinkedList *Node;

Node ll_create_node() {
   Node node = malloc(sizeof(struct LinkedList));
   node->next = NULL;
   return node;
}

int main() {

    int N;
    printf("Enter the value of n:\t\t");
    read_uint(&N);

    printf("Enter the elements:\t\t");

    Node head = NULL;
    if(N>0) {
        head = ll_create_node();
        read_int(&(head->data));
    }

    Node current = head;
    for(int i = 1; i < N; i++) {
        current->next = ll_create_node();
        current = current->next;
        read_int(&(current->data));
    }

    int K;
    printf("Enter the value of n:\t\t");
    read_nzint(&K);

    if(K>N) {
        printf("Error: K cannot be greater than N, N is %d\n", N);
        return 1;
    }

    current = head;
    for(int i = 0; i < N-K; i++) {
        current = current->next;
    }

    printf("The ");
    switch(K) {
        case 1: printf("1st"); break;
        case 2: printf("2nd"); break;
        case 3: printf("3rd"); break;
        default: printf("%dth", K); break;
    }
    printf(" element of the list is:\t%d\n", current->data);

    return 0;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("Error: Please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("Error: Please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("Error: Please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
