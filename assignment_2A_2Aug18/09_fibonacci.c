#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/**
 * Fibonacci series using recursion.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

unsigned long long int fib(int n);

int main() {

    int n;
    printf("Enter the number(n)\t= ");
    read_nzint(&n);

    printf("Fibonacci series\t");
    int fib_n_1_last_digit = 0;
    int fib_n_2_last_digit = 0;
    int fib_last_digit;
    for(int i = 1; i<n; i++) {
        fib_last_digit = fib(i)%10;
        if(i>2 && fib_last_digit != (fib_n_1_last_digit + fib_n_2_last_digit) % 10) {
            printf(" ...\nCalculation overflow.\n");
            return 1;
        }
        fib_n_2_last_digit = fib_n_1_last_digit;
        fib_n_1_last_digit = fib_last_digit;
        printf("%llu, ", fib(i));
    }
    printf("%llu\n", fib(n));

    return 0;
}

unsigned long long fib(int n) {
    static unsigned long long int memo[10000];

    if(n<=1) return 0;
    if(n==2) return 1;
    if(n<10000) {
        if(memo[n]==0) {
            memo[n] = fib(n-1) + fib(n-2);
        }
        return memo[n];
    } else {
        return fib(n-1) + fib(n-2);
    }
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
