#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

void print_nature_of_roots(int a, int b, int c);
void print_roots(int a, int b, int c);

int main() {

    int a, b, c;
    printf("Enter a, b, c\t: ");
    read_int(&a); read_int(&b); read_int(&c);

    print_nature_of_roots(a, b, c);

    print_roots(a, b, c);
    printf("\n");

    return 0;
}

void print_nature_of_roots(int a, int b, int c) {
    if(a==0 && b==0 && c==0) {
        printf("All real and imaginary numbers are solutions.\n");
    } else if (a==0 && b==0) {
        printf("No solution. a & b both zero.\n");
    } else if (a==0) {
        printf("Linear equation.\n");
    } else if (b*b - 4*a*c >= 0) {
        printf("Roots are real.\n");
    } else {
        printf("Roots are complex.\n");
    }
}

void print_roots(int a, int b, int c) {
    if(a==0 && b==0) {
        return;
    }
    printf("Roots\t\t: ");
    if(a==0) {
        float root = -(float)c/b;
        printf("%f", root);
    } else if(b*b == 4*a*c) {
        float root = -(float)b/(2*a);
        if(fpclassify(root) == FP_ZERO) {
            root = 0;
        }
        printf("%f", root);
    } else {
        float D = b*b - 4*a*c;
        float sqrt_D = sqrtf(fabsf(D));
        if(D>0) {
            printf("%.3f and %.3f", (-b+sqrt_D)/(2*a), (-b-sqrt_D)/(2*a));
        } else {
            printf("%.3f + %.3fi and %.3f - %.3fi", -(float)b/(2*a), sqrt_D/(2*a), -(float)b/(2*a), sqrt_D/(2*a));
        }
    }

    printf("\n");
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
