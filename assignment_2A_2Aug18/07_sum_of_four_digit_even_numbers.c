#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Find the sum of four digit even numbers in a list.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

void print_sum_of_four_digit_even_numbers(const int *array, int len);


int main() {

    int n;
    printf("Enter the number of elements\t: ");
    read_nzint(&n);

    int A[n];
    printf("Enter the elements\t\t: ");
    for(int i=0;i<n;i++) {
        read_int(&A[i]);
    }

    printf("The sum is\t\t\t: ");
    print_sum_of_four_digit_even_numbers(A, n);
    printf("\n");

    return 0;
}

void print_sum_of_four_digit_even_numbers(const int *array, int len) {
    int sum = 0;
    int num_four_digit_even_nums = 0;
    for(int i=0;i<len;i++) {
        if((
             (array[i]<=9999 && array[i]>=1000) ||
             (array[i]>=-9999 && array[i] <=-1000)
           ) &&
           array[i]%2==0 ) {
            if(num_four_digit_even_nums > 0) printf(" +");
            printf(" %d", array[i]);
            sum += array[i];
            num_four_digit_even_nums++;
        }
    }
    if(num_four_digit_even_nums==0) printf("0");
    else if(num_four_digit_even_nums>=2) printf(" = %d", sum);
}

/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
