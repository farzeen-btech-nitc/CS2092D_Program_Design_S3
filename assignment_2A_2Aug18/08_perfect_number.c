#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
void read_llint   (long long int *var);
void read_ullint  (long long int *var);

bool is_perfect_num(long long int num);

int main() {
    long long int n;
    printf("Enter an integer\t: ");
    read_ullint(&n);
    if(is_perfect_num(n)) {
        printf("%d is a perfect number.\n", n);
    } else {
        printf("%d is not a perfect number.\n", n);
    }
    return 0;
}


bool is_perfect_num(long long int num) {
    long long int sum = 0;
    for(long long int i=1;i<num; i++) {
        if(num%i==0) sum+=i;
        if(sum>num) break;
    }
    return sum == num;
}

/* impl utility functions ---------------------- */

void read_llint   (long long int *var) {
    errno = 0;
    if(!scanf("%lld,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_ullint  (long long int *var) {
    read_llint(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}
