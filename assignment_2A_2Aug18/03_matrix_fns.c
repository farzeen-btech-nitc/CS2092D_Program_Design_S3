#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_nzint (int *var);

/* matrix data type ---------------------------- */
struct Matrix {
    int *data;
    int rows, cols;
};
typedef struct Matrix Matrix;

/* matrix fns ---------------------------------- */
void read       (int *X, int m, int n);
void print      (const int *X, int m, int n);
Matrix sum      (Matrix A, Matrix B);
Matrix subtract (Matrix A, Matrix B);


int main() {
    Matrix A;
    Matrix B;

    printf("Enter the order of matrix A:\t");
    read_nzint(&A.rows); read_nzint(&A.cols);
    A.data = malloc(sizeof(int)*A.rows*A.cols);

    printf("Enter the order of matrix B:\t");
    read_nzint(&B.rows); read_nzint(&B.cols);
    B.data = malloc(sizeof(int)*B.rows*B.cols);

    printf("\n");

    printf("Enter the elements of matrix A:\t");
    read(A.data, A.rows, A.cols);
    printf("\n");

    printf("Enter the elements of matrix B:\t");
    read(B.data, B.rows, B.cols);
    printf("\n");

    printf("Matrix A is: ");
    print(A.data, A.rows, A.cols);

    printf("Matrix B is: ");
    print(B.data, B.rows, B.cols);

    if(A.rows != B.rows || A.cols != B.cols) {
        printf("Matix addition not possible.\n");
        printf("Matix subtraction not possible.\n");
    } else {
        printf("Matrix C is: ");
        Matrix C = sum(A, B);
        print(C.data, C.rows, C.cols);

        printf("Matrix D is: ");
        Matrix D = subtract(A, B);
        print(D.data, D.rows, D.cols);
    }

    return 0;
}


/* impl matrix functions ----------------------- */
void read(int *X, int m, int n) {
    for(int i=0;i<m; i++) {
        if(i!=0) printf("\t\t\t\t");
        for(int j=0; j<n; j++) {
            read_int(X+m*i+j);
        }
    }
}

void print(const int *X, int m, int n) {
    printf("\t\t\t");
    for(int i=0; i<m; i++) {
        if(i!=0) {printf("\t\t\t\t");}
        for(int j=0; j<n; j++) {
            printf("%d\t", *(X+m*i+j));
        }
        printf("\n");
    }
    printf("\n");
}

Matrix sum(Matrix A, Matrix B) {
    if(A.rows!=B.rows||A.cols!=B.cols) {
        printf("ERROR: Can't sum two matrices of different dimensions.\n");
        Matrix ret = {0,0,0};
        return ret;
    }
    Matrix ret = {
        .data=malloc(sizeof(int)*A.cols*A.rows),
        .rows=A.rows,
        .cols=A.cols
    };

    for(int i=0;i<A.rows; i++) {
        for(int j=0; j<A.cols; j++) {
            *(ret.data+A.cols*i+j) = *(A.data+A.cols*i+j) + *(B.data+B.cols*i+j);
        }
    }
    return ret;
}

Matrix subtract(Matrix A, Matrix B) {
    if(A.rows!=B.rows||A.cols!=B.cols) {
        printf("ERROR: Can't subtract two matrices of different dimensions.\n");
        Matrix ret = {0,0,0};
        return ret;
    }
    Matrix ret = {
        .data=malloc(sizeof(int)*A.cols*A.rows),
        .rows=A.rows,
        .cols=A.cols
    };

    for(int i=0;i<A.rows; i++) {
        for(int j=0; j<A.cols; j++) {
            *(ret.data+A.cols*i+j) = *(A.data+A.cols*i+j) - *(B.data+B.cols*i+j);
        }
    }
    return ret;
}

/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
