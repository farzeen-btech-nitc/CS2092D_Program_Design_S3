#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Convert a positive decimal fraction to binary.
 */

/* Read the fractional part of a positive decimal fraction as int */
float read_positive_decimal_frac();

int main() {
    float decimal_frac = read_positive_decimal_frac();
    float bin_frac = 0;
    float to_add = 0.1;
    for(int i=0;i<4;i++) {
        decimal_frac*=2;
        if(decimal_frac>=1) {
            bin_frac+=to_add;
            decimal_frac-=1;
        }
        to_add/=10;
    }
    printf("Binary equivalent of %f is\t\t: %.4f", decimal_frac, bin_frac);
    if(decimal_frac>0) printf("...");
    printf("\n");
    return 0;
}

float read_positive_decimal_frac() {
    float f;
    printf("Enter a positive decimal fraction less than 1\t: ");
    if(!scanf("%f", &f)||errno) {
        printf("ERROR: Invalid input.\n");
        exit(1);
    }
    if(f<0||f>=1) {
        printf("Entered number is not a positive decimal fraction less than 1\n");
        exit(1);
    }
}

