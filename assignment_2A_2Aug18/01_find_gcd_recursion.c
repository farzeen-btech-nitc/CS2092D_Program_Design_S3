#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Find GCD using recursion.
 */

void read_int   (int *var);
int  gcd        (int a, int b);

int main() {

    int a,b;
    printf("Enter two integers:\t");
    read_int(&a); read_int(&b);
    int _gcd = gcd(a,b);
    printf("GCD of %d and %d:\t%d\n",a,b,_gcd);

    return 0;
}

/* Notes: The following must hold true.
 * 1.   a => b
 * 2.   a, b > 0
 */
int gcd_recurse(int min, int max) {
    if(min==1||max==1) return 1;

    int divisor = 2;
    while(divisor<=min) {
        if(max % divisor == 0) {
            if(min % divisor == 0) {
                return divisor * gcd_recurse(min/divisor, max/divisor);
            } else {
                return gcd_recurse(min, max/divisor);
            }
        } else if(min % divisor == 0) {
            return gcd_recurse(min/divisor, max);
        }
        divisor ++;
    }

    return 1;
}

int gcd(int a, int b) {
    if(a<0) a = -a; if(b<0) b = -b;
    if(a==b) return a;
    int min = a<b?a:b;
    int max = a>b?a:b;
    if(a==0||b==0) return max;
    return gcd_recurse(min, max);
}

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}
