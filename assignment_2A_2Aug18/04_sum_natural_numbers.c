#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Sum of natural numbers, which are multiple of x and y
 */
struct IntVector {
    int *data;
    int len;
    int capacity;
};
typedef struct IntVector IntVector;

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

IntVector get_multiples_of_x_below_n    (int x, int n);
void      print_vector                  (IntVector vector);
int       sum_vector                    (IntVector vector);

IntVector vector_new                    ();
void      vector_append                 (IntVector *vector, int item);

int main() {

    int n, x, y;

    printf("Enter the value of n\t: ");
    read_nzint(&n);

    printf("Enter the value of x\t: ");
    read_nzint(&x);

    printf("Enter the value of y\t: ");
    read_nzint(&y);

    IntVector x_multiples = get_multiples_of_x_below_n(x, n);
    IntVector y_multiples = get_multiples_of_x_below_n(y, n);
    IntVector xy_multiples = get_multiples_of_x_below_n(x*y, n);


    printf("Multiples of %d are\t: ",x);
    print_vector(x_multiples);
    printf("\n");

    printf("Multiples of %d are\t: ",y);
    print_vector(y_multiples);
    printf("\n");


    int sum_multiples = sum_vector(x_multiples) + sum_vector(y_multiples) - sum_vector(xy_multiples);
    printf("Total sum is\t: %d\n", sum_multiples);

    return 0;
}

IntVector get_multiples_of_x_below_n    (int x, int n) {
    IntVector vector = vector_new();
    int max_i = n/x;
    for(int i = 1; i<=max_i; i++) {
        vector_append(&vector, x*i);
    }
    return vector;
}

void print_vector(IntVector vector) {
    for(int i=0;i<=vector.len-2; i++) {
        printf("%d, ", vector.data[i]);
    }
    printf("%d", vector.data[vector.len-1]);
}

int sum_vector(IntVector vector) {
    int sum = 0;
    for(int i=0;i<vector.len; i++) {
        sum+=vector.data[i];
    }
    return sum;
}

IntVector vector_new() {
    IntVector v = {.data=malloc(sizeof(int)*5), .len = 0, .capacity = 5};
    return v;
}

void vector_append(IntVector *vector, int item) {
    if(vector->len == vector->capacity) {
        int *data_copy = malloc(sizeof(int)*vector->capacity*2);
        memcpy(data_copy, vector->data, vector->len);
        free(vector->data);
        vector->data = data_copy;
        vector->capacity *= 2;
    }

    vector->data[vector->len] = item;
    vector->len++;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
