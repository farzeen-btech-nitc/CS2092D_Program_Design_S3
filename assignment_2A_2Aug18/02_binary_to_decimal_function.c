#include <stdio.h>
#include <stdlib.h>

/**
 * Convert binary number to decimal, write a function.
 */

/* Reads a string of 0's and 1's returns it,
 * converting it on the fly. */
int read_binary_num();


int main() {

    printf("Enter a binary number\t\t: ");
    int num = read_binary_num();
    printf("The equivalent decimal number\t: %d\n", num);

    return 0;
}

int read_binary_num() {
    unsigned int num = 0;
    char ch;
    while(1) {
        ch = getchar();
        switch(ch) {
            case '1' : num *= 2;
                       num += 1;
                       break;
            case '0' : num *= 2;
                       break;
            case '\n': return num;
            default  : printf("ERROR: Unexpected character '%c'. \n", ch);
                       exit(1);
        }
    }
}

