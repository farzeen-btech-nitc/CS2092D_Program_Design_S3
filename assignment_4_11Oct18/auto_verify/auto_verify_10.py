import sys
from subprocess import run
from random import uniform
from os import chdir

c_file = '../'+sys.argv[1]
infile = sys.argv[2]

run('mkdir -p tests'.split())
chdir('tests')

run(['gcc', '-g', '-o', 'test.out', c_file], check=True)

r = [int(uniform(-300, 300)) for i in range(0, 3000)]
with open(infile, 'w') as f:
    f.write("{}\n".format(len(r)))
    f.writelines(" ".join([str(x) for x in r]).strip())

p = run(['./test.out'], check=True, capture_output=True, text=True)
output = p.stdout.splitlines()[2].strip()

r.sort()
if output != " ".join([str(x) for x in r]).strip():
    print("Error:")
    print(" Expected: ", " ".join([str(x) for x in r]).strip())
    print(" Recieved: ", output)
else:
    print("Sort test passed!")


r.sort()
with open(infile, 'w') as f:
    f.write("{}\n".format(len(r)))
    f.writelines(" ".join([str(x) for x in r]).strip())

p = run(['./test.out'], check=True, capture_output=True, text=True)
output = p.stdout

if "ascending" not in output:
    print("Error:")
    print(" Expected: ", "ascending")
    print(" Recieved: ", output)
else:
    print("Ascending test passed!")

r.reverse()
with open(infile, 'w') as f:
    f.write("{}\n".format(len(r)))
    f.writelines(" ".join([str(x) for x in r]).strip())

p = run(['./test.out'], check=True, capture_output=True, text=True)
output = p.stdout

if "descending" not in output:
    print("Error:")
    print(" Expected: ", "descending")
    print(" Recieved: ", output)
else:
    print("Descending test passed!")
