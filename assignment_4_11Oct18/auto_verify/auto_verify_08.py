import sys
from subprocess import run
from random import uniform
from os import chdir

c_file = '../'+sys.argv[1]
infile = sys.argv[2]
outfile = sys.argv[3]

run('mkdir -p tests'.split())
chdir('tests')

run(['gcc', '-g', '-o', 'test.out', c_file], check=True)

with open(infile, 'w') as f:
    r = [int(uniform(-300, 300)) for i in range(0, 30)]
    r.sort()
    s = [int(uniform(-300, 300)) for i in range(0, 10)]
    s.sort()
    f.write("{}\n".format(len(r)))
    f.write("{}\n".format(len(s)))
    f.write(" ".join([str(x) for x in r]).strip())
    f.write(" ".join([str(x) for x in s]).strip())

t = r+s
t.sort()

p = run(['./test.out'], check=True)
with open(outfile, 'r') as f:
    output = f.readline().strip()
    if output != " ".join(map(str, t)).strip():
        print("Error:")
        print(" Expected: ", " ".join(map(str, t)).strip())
        print(" Recieved: ", output)
    else:
        print("Test passed")
