#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

void read_file(const char* filename, int* array, int* n);
void print_array(FILE* fp, int* array, int n);

void bucket_sort(int* array, int n)
{
    int buckets[n][n];
    int b_len[n];
    for (int i = 0; i < n; i++) {
        b_len[i] = 0;
    }
    for (int i = 0; i < n; i++) {
        if (array[i] == INT_MAX)
            continue;
        int f = 1;
        for (int j = i + 1; j < n; j++) {
            if (array[j] == array[i]) {
                f++;
                array[j] = INT_MAX;
            }
        }
        for (int j = 0; j < f; j++) {
            buckets[f][b_len[f]] = array[i];
            b_len[f]++;
        }
        array[i] = INT_MAX;
    }

    /* for (int i = 0; i < n; i++) { */
    /*     if (b_len[i] == 0) */
    /*         continue; */
    /*     printf("%d: ", i); */
    /*     print_array(stdout, buckets[i], b_len[i]); */
    /*     printf("\n\n"); */
    /* } */

    int k = 0;
    for (int i = n - 1; i >= 0; i--) {
        if (b_len[i] == 0)
            continue;
        for (int j = 0; j < b_len[i]; j++, k++) {
            array[k] = buckets[i][j];
        }
    }
}

void main()
{
    int array[100000];
    int n;
    read_file("buckin.txt", array, &n);
    bucket_sort(array, n);
    FILE* outfile = fopen("buckout.txt", "w");
    print_array(outfile, array, n);
    fflush(outfile);
    fclose(outfile);
}

void read_file(const char* filename, int* array, int* n)
{
    FILE* f = fopen(filename, "r");
    if (f == NULL) {
        printf("'%s' not found.\n", filename);
        return;
    }
    *n = 0;
    while (fscanf(f, "%d", &array[*n]) == 1)
        (*n)++;
    fclose(f);
}

void print_array(FILE* fp, int* a, int n)
{
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", a[i]);
    }
}
