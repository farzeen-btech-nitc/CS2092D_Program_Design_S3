#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

void read_file(const char* filename, int** a, int* m, int** b, int* n);
void print_array(FILE* fp, int* array, int n);

// NOTE: Sentinel expected: a[m] = b[n] = INT_MAX
void merge(int* a, int m, int* b, int n, int* c)
{
    int i = 0, j = 0;
    while (i + j < m + n) {
        if (a[i] <= b[j]) {
            c[i + j] = a[i];
            i++;
        } else {
            c[i + j] = b[j];
            j++;
        }
    }
}

int sorted_asc(int* a, int n)
{
    for (int i = 0; i < n - 1; i++) {
        if (a[i] > a[i + 1])
            return 0;
    }
    return 1;
}

void main()
{
    int* a;
    int m;
    int* b;
    int n;
    read_file("8_1.txt", &a, &m, &b, &n);
    int c[m + n];

    if (!sorted_asc(a, m) || !sorted_asc(b, n)) {
        printf("The array is not sorted in ascending order\n");
        return;
    }
    merge(a, m, b, n, c);
    FILE* f = fopen("8_2.txt", "w");
    print_array(f, c, m + n);
    fclose(f);
    free(a);
    free(b);
}

void read_file(const char* filename, int** a, int* m, int** b, int* n)
{
    FILE* f = fopen(filename, "r");
    if (!f)
        printf("'%s' not found.\n");
    fscanf(f, "%d", m);
    fscanf(f, "%d", n);

    *a = malloc((*m + 1) * sizeof(int));
    *b = malloc((*n + 1) * sizeof(int));

    for (int i = 0; i < *m; i++)
        fscanf(f, "%d", &(*a)[i]);

    for (int i = 0; i < *n; i++)
        fscanf(f, "%d", &(*b)[i]);

    (*a)[*m] = (*b)[*n] = INT_MAX;

    fclose(f);
}

void print_array(FILE* fp, int* a, int n)
{
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", a[i]);
    }
}
