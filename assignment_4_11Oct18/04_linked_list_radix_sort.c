#include <stdio.h>
#include <stdlib.h>

struct Node {
    int data;
    struct Node* next;
};
typedef struct Node Node;

void read_file(const char* filename, Node** head_ptr, int* n);
void print_list(FILE* fp, Node* head);

void radix_sort(Node* head, int n)
{
    int buckets[20][n];
    int b_len[20];
    int _continue = 1;
    for (int pow = 1; _continue; pow *= 10) {
        for (int i = 0; i < 20; i++) {
            b_len[i] = 0;
        }

        _continue = 0;
        for (Node* c = head; c != NULL; c = c->next) {
            _continue = _continue || c->data > pow * 10;
            int b = 9 + ((c->data / pow) % 10);

            /* printf("%d: %d\n", b, array[i]); */
            buckets[b][b_len[b]] = c->data;
            b_len[b]++;
        }

        Node* c = head;
        for (int i = 19; i >= 0; i--) {
            for (int j = 0; j < b_len[i]; j++) {
                c->data = buckets[i][j];
                c = c->next;
            }
        }
    }
}

void main()
{
    int n;
    Node* head;
    read_file("radin.txt", &head, &n);
    radix_sort(head, n);
    FILE* outfile = fopen("radout.txt", "w");
    print_list(outfile, head);
    fflush(outfile);
    fclose(outfile);
}

void read_file(const char* filename, Node** head_ptr, int* n)
{
    FILE* f = fopen(filename, "r");
    if (!f)
        printf("'%s' not found.\n");
    *n = 0;
    Node head;
    Node* c = &head;
    int data;
    while (fscanf(f, "%d", &data) == 1) {
        c->next = malloc(sizeof(Node));
        c = c->next;
        c->data = data;
        (*n)++;
    }
    fclose(f);
    *head_ptr = head.next;
}

void print_list(FILE* fp, Node* head)
{
    for (Node* c = head; c != NULL; c = c->next) {
        fprintf(fp, "%d ", c->data);
    }
}
