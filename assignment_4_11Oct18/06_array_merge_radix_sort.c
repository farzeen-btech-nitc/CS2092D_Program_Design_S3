#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

void read_file(const char* filename, int* a, int* m, int* b, int* n);
void print_array(FILE* fp, int* array, int n);

void radix_sort(int* array1, int n)
{
    int buckets[20][n];
    int b_len[20];
    int _continue = 1;
    for (int pow = 1; _continue; pow *= 10) {
        for (int i = 0; i < 20; i++) {
            b_len[i] = 0;
        }

        _continue = 0;
        for (int i = 0; i < n; i++) {
            _continue = _continue || array1[i] > pow * 10;
            int b = 9 + ((array1[i] / pow) % 10);

            /* printf("%d: %d\n", b, array1[i]); */
            buckets[b][b_len[b]] = array1[i];
            b_len[b]++;
        }

        int k = 0;
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < b_len[i]; j++, k++) {
                array1[k] = buckets[i][j];
            }
        }
    }
}

void main()
{
    int a[100000];
    int b[100000];
    int m, n;
    read_file("sortin.txt", a, &m, b, &n);

    int l = 0;
    for (int j = 0; j < n; j++) {
        for (int i = 0; i < m; i++) {
            if (a[i] == b[j]) {
                int temp = a[l];
                a[l] = a[i];
                a[i] = temp;
                l++;
            }
        }
    }
    radix_sort(a + l, m - l);

    FILE* outfile = fopen("sortout.txt", "w");
    print_array(outfile, a, m);
    fflush(outfile);
    fclose(outfile);
}

void read_file(const char* filename, int* a, int* m, int* b, int* n)
{
    FILE* f = fopen(filename, "r");
    if (!f)
        printf("'%s' not found.\n");
    *m = 0;
    char ch;
    while ((ch = fgetc(f)) != '\n') {
        ungetc(ch, f);
        fscanf(f, "%d", &a[*m]);
        (*m)++;
    }
    *n = 0;
    while ((ch = fgetc(f)) != '\n') {
        ungetc(ch, f);
        fscanf(f, "%d", &b[*n]);
        (*n)++;
    }
    fclose(f);
}

void print_array(FILE* fp, int* a, int n)
{
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", a[i]);
    }
}
