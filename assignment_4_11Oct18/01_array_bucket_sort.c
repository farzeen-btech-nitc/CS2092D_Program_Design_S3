#include <stdio.h>
#include <stdlib.h>

void read_file(const char* filename, int* array, int* n);
void print_array(FILE* fp, int* array, int n);

void insertion_sort(int* a, int n);
void bucket_sort(int* array, int n)
{
    int*(buckets[100]);
    int b_len[100];
    for (int i = 0; i < 100; i++) {
        buckets[i] = malloc(n * sizeof(int));
        b_len[i] = 0;
    }

    int min = array[0];
    int max = array[0];
    for (int i = 1; i < n; i++) {
        if (array[i] < min)
            min = array[i];
        if (array[i] > max)
            max = array[i];
    }

#define B(x) ((x - min) * 99 / (max - min))
    for (int i = 0; i < n; i++) {
        int b = B(array[i]);
        /* printf("%d: %d\n", b, array[i]); */
        buckets[b][b_len[b]] = array[i];
        b_len[b]++;
    }

    for (int i = 0; i < 100; i++) {
        if (b_len[i] == 0)
            continue;
        /* printf("%d: ", i); */
        /* print_array(stdout, buckets[i], b_len[i]); */
        /* printf("\n\n"); */
        insertion_sort(buckets[i], b_len[i]);
    }

    int k = 0;
    for (int i = 0; i < 100; i++) {
        if (b_len[i] == 0)
            continue;
        for (int j = 0; j < b_len[i]; j++, k++) {
            array[k] = buckets[i][j];
        }
        free(buckets[i]);
    }
}

void insertion_sort(int* a, int n)
{
    for (int i = 1; i < n; i++) {
        int key = i;
        int j = i - 1;
        while (j >= 0 && a[j] > a[key]) {
            int temp = a[j];
            a[j] = a[key];
            a[key] = temp;
            key = j;
            j--;
        }
    }
}

void main()
{
    int array[100000];
    int n;
    read_file("buckin.txt", array, &n);
    bucket_sort(array, n);
    FILE* outfile = fopen("buckout.txt", "w");
    print_array(outfile, array, n);
    fflush(outfile);
    fclose(outfile);
}

void read_file(const char* filename, int* array, int* n)
{
    FILE* f = fopen(filename, "r");
    if (!f)
        printf("'%s' not found.\n");
    *n = 0;
    while (fscanf(f, "%d", &array[*n]) == 1)
        (*n)++;
    fclose(f);
}

void print_array(FILE* fp, int* a, int n)
{
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", a[i]);
    }
}
