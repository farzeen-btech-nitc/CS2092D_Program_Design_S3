#include <stdio.h>
#include <stdlib.h>

struct Node {
    int data;
    struct Node* next;
};
typedef struct Node Node;

void read_file(const char* filename, Node** head_ptr, int* n);
void print_list(FILE* fp, Node* head);

void insertion_sort(int* a, int n);
void bucket_sort(Node* head, int n)
{
    int*(buckets[100]);
    int b_len[100];
    for (int i = 0; i < 100; i++) {
        buckets[i] = malloc(n * sizeof(int));
        b_len[i] = 0;
    }

    int min = head->data;
    int max = head->data;

    for (Node* cursor = head; cursor != NULL; cursor = cursor->next) {
        if (cursor->data < min)
            min = cursor->data;
        if (cursor->data > max)
            max = cursor->data;
    }

#define B(x) ((x - min) * 99 / (max - min))
    for (Node* c = head; c != NULL; c = c->next) {
        int b = B(c->data);
        /* printf("%d: %d\n", b, array[i]); */
        buckets[b][b_len[b]] = c->data;
        b_len[b]++;
    }

    for (int i = 0; i < 100; i++) {
        if (b_len[i] == 0)
            continue;
        /* printf("%d: ", i); */
        /* print_array(stdout, buckets[i], b_len[i]); */
        /* printf("\n\n"); */
        insertion_sort(buckets[i], b_len[i]);
    }

    Node* c = head;
    for (int i = 0; i < 100; i++) {
        if (b_len[i] == 0)
            continue;
        for (int j = 0; j < b_len[i]; j++) {
            c->data = buckets[i][j];
            c = c->next;
        }
        free(buckets[i]);
    }
}

void insertion_sort(int* a, int n)
{
    for (int i = 1; i < n; i++) {
        int key = i;
        int j = i - 1;
        while (j >= 0 && a[j] > a[key]) {
            int temp = a[j];
            a[j] = a[key];
            a[key] = temp;
            key = j;
            j--;
        }
    }
}

void main()
{
    int n;
    Node* head;
    read_file("buckin.txt", &head, &n);
    bucket_sort(head, n);
    FILE* outfile = fopen("buckout.txt", "w");
    print_list(outfile, head);
    fflush(outfile);
    fclose(outfile);
}

void read_file(const char* filename, Node** head_ptr, int* n)
{
    FILE* f = fopen(filename, "r");
    if (!f)
        printf("'%s' not found.\n");
    *n = 0;
    Node head;
    Node* c = &head;
    int data;
    while (fscanf(f, "%d", &data) == 1) {
        c->next = malloc(sizeof(Node));
        c = c->next;
        c->data = data;
        (*n)++;
    }
    fclose(f);
    *head_ptr = head.next;
}

void print_list(FILE* fp, Node* head)
{
    for (Node* c = head; c != NULL; c = c->next) {
        fprintf(fp, "%d ", c->data);
    }
}
