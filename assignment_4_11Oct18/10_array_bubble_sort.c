#include <stdio.h>
#include <stdlib.h>

void read_file(const char* filename, int* array, int* n);
void print_array(FILE* fp, int* array, int n);

void bubble_sort(int* a, int n)
{
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++)
            if (a[j] > a[j + 1]) {
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
    }
}

int sorted_asc(int* a, int n)
{
    for (int i = 0; i < n - 1; i++) {
        if (a[i] > a[i + 1])
            return 0;
    }
    return 1;
}

int sorted_desc(int* a, int n)
{
    for (int i = 0; i < n - 1; i++) {
        if (a[i] < a[i + 1])
            return 0;
    }
    return 1;
}

void main()
{
    int array[100000];
    int n;
    read_file("10.txt", array, &n);
    if (sorted_asc(array, n)) {
        printf("The array is sorted in ascending order\n");
        return;
    }
    if (sorted_desc(array, n)) {
        printf("The array is sorted in descending order\n");
        return;
    }
    printf("The array is not sorted.\n");
    bubble_sort(array, n);
    printf("The sorted array in ascending order:\n");
    print_array(stdout, array, n);
}

void read_file(const char* filename, int* array, int* n)
{
    FILE* f = fopen(filename, "r");
    fscanf(f, "%d", n);
    if (!f)
        printf("'%s' not found.\n");
    *n = 0;
    while (fscanf(f, "%d", &array[*n]) == 1)
        (*n)++;
    fclose(f);
}

void print_array(FILE* fp, int* a, int n)
{
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", a[i]);
    }
}
