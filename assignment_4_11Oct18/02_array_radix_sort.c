#include <stdio.h>
#include <stdlib.h>

void read_file(const char* filename, int* array, int* n);
void print_array(FILE* fp, int* array, int n);

void radix_sort(int* array, int n, FILE* out)
{
    int buckets[20][n];
    int b_len[20];
    int _continue = 1;
    for (int pow = 1; _continue; pow *= 10) {
        for (int i = 0; i < 20; i++) {
            b_len[i] = 0;
        }

        _continue = 0;
        for (int i = 0; i < n; i++) {
            _continue = _continue || array[i] > pow * 10;
            int b = 9 + ((array[i] / pow) % 10);

            /* printf("%d: %d\n", b, array[i]); */
            buckets[b][b_len[b]] = array[i];
            b_len[b]++;
        }

        int k = 0;
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < b_len[i]; j++, k++) {
                array[k] = buckets[i][j];
            }
        }
        print_array(out, array, n);
        fprintf(out, "\n");
    }
}

void main()
{
    int array[100000];
    int n;
    read_file("radix.txt", array, &n);
    FILE* outfile = fopen("radout.txt", "w");
    radix_sort(array, n, outfile);
    fflush(outfile);
    fclose(outfile);
}

void read_file(const char* filename, int* array, int* n)
{
    FILE* f = fopen(filename, "r");
    if (!f)
        printf("'%s' not found.\n");
    *n = 0;
    while (fscanf(f, "%d", &array[*n]) == 1)
        (*n)++;
    fclose(f);
}

void print_array(FILE* fp, int* a, int n)
{
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", a[i]);
    }
}
