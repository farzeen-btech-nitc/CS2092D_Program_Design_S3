#include <stdio.h>
#include <stdlib.h>

struct Node {
    int data;
    struct Node* next;
};
typedef struct Node Node;

void read_file(const char* filename, Node** head_ptr, int* n);
void print_list(FILE* fp, Node* head);

void selection_sort(Node* head)
{
    for (Node* c = head; c->next != NULL; c = c->next) {
        Node* min = c;
        for (Node* c1 = c->next; c1 != NULL; c1 = c1->next) {
            if (c1->data < min->data)
                min = c1;
        }
        int temp = c->data;
        c->data = min->data;
        min->data = temp;
        print_list(stdout, head);
        printf("\n\n");
    }
}

void main()
{
    int n;
    Node* head;
    read_file("9_1.txt", &head, &n);
    selection_sort(head);
    FILE* outfile = fopen("9_2.txt", "w");
    print_list(outfile, head);
    fflush(outfile);
    fclose(outfile);
}

void read_file(const char* filename, Node** head_ptr, int* n)
{
    FILE* f = fopen(filename, "r");
    if (!f)
        printf("'%s' not found.\n");
    *n = 0;
    Node head;
    Node* c = &head;
    int data;
    while (fscanf(f, "%d", &data) == 1) {
        c->next = malloc(sizeof(Node));
        c = c->next;
        c->data = data;
        (*n)++;
    }
    fclose(f);
    *head_ptr = head.next;
}

void print_list(FILE* fp, Node* head)
{
    for (Node* c = head; c != NULL; c = c->next) {
        fprintf(fp, "%d ", c->data);
    }
}
