#include <stdio.h>
#include <stdlib.h>

void read_file(const char* filename, int* array, int* n, int* k);
void print_array(FILE* fp, int* array, int n);

void radix_sort(int* array, int n, int ascending)
{
    int buckets[20][n];
    int b_len[20];
    int _continue = 1;
    for (int pow = 1; _continue; pow *= 10) {
        for (int i = 0; i < 20; i++) {
            b_len[i] = 0;
        }

        _continue = 0;
        for (int i = 0; i < n; i++) {
            _continue = _continue || array[i] > pow * 10;
            int b = 9 + ((array[i] / pow) % 10);

            /* printf("%d: %d\n", b, array[i]); */
            buckets[b][b_len[b]] = array[i];
            b_len[b]++;
        }

        int k = 0;
        if (ascending == 1) {
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < b_len[i]; j++, k++) {
                    array[k] = buckets[i][j];
                }
            }
        } else {
            for (int i = 19; i >= 0; i--) {
                for (int j = 0; j < b_len[i]; j++, k++) {
                    array[k] = buckets[i][j];
                }
            }
        }
    }
}

void main()
{
    int array[100000];
    int n;
    int k;
    read_file("radin.txt", array, &n, &k);
    radix_sort(array, k, 1);
    radix_sort(array + k, n - k, 0);
    FILE* outfile = fopen("radout.txt", "w");
    print_array(outfile, array, n);
    fflush(outfile);
    fclose(outfile);
}

void read_file(const char* filename, int* array, int* n, int* k)
{
    FILE* f = fopen(filename, "r");
    if (!f)
        printf("'%s' not found.\n");
    *n = 0;
    while (fscanf(f, "%d", &array[*n]) == 1)
        (*n)++;
    (*n)--;
    *k = array[*n];
    fclose(f);
}

void print_array(FILE* fp, int* a, int n)
{
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", a[i]);
    }
}
