#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Tower of Hanoi.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

struct Stack {
    int *data;
    int top;
    int capacity;
};
typedef struct Stack Stack;
Stack stack_new      (int capacity);
void  stack_push     (Stack *self, int item);
int   stack_pop      (Stack *self);
bool  stack_is_empty (Stack *self);
bool  stack_equals   (Stack *self, Stack *other);
Stack stack_clone    (Stack *self);
void  stack_print    (Stack *self);
void  stack_free     (Stack *self);

struct Configuration {
    Stack a;
    Stack b;
    Stack c;
};
typedef struct Configuration Configuration;
Configuration  configuration_clone (Configuration *self);
bool           configuration_equals(Configuration *self, Configuration *other);
void           configuration_free(Configuration *self);

struct ConfigurationList {
    Configuration *configs;
    int   len;
    int   capacity;
};
typedef struct ConfigurationList ConfigurationList;
ConfigurationList config_list_new();
void  config_list_push_clone (ConfigurationList *self, Configuration *config);
bool  config_list_contains   (ConfigurationList *self, Configuration *config);
ConfigurationList
      config_list_clone    (ConfigurationList *self);
void  config_list_free (ConfigurationList *self);

/* Parameters:
 *      initial : Intial configuration.
 *      final   : Final configuration.
 *      recurse_base_config_list: Used for recursion. Should be NULL for first call.
 *      None of the above parameters are modified by this function.
 * Returns: A ConfigurationList with least number of elements possible. */
ConfigurationList  tower_of_hanoi (Configuration initial, Configuration final, ConfigurationList *recurse_base_config_list);

int main() {
    int N;
    printf("Enter N\t: ");
    read_uint(&N);
    Configuration initial = {.a = stack_new(N), .b = stack_new(N), .c = stack_new(N)};
    Configuration final = {.a = stack_new(N), .b = stack_new(N), .c = stack_new(N)};
    for(int i = N; i>=1; i--) {
        stack_push(&initial.a, i);
        stack_push(&final.c, i);
    }

    ConfigurationList list_of_steps = tower_of_hanoi(initial, final, NULL);
    printf("Number of steps required: %d.\n", list_of_steps.len);

        printf("A: ");
        stack_print(&initial.a);
        printf("\n");
        printf("B: ");
        stack_print(&initial.b);
        printf("\n");
        printf("C: ");
        stack_print(&initial.c);
        printf("\n");
        printf("----------------------\n");

    for(int i=0; i<list_of_steps.len; i++) {
        printf("A: ");
        stack_print(&list_of_steps.configs[i].a);
        printf("\n");
        printf("B: ");
        stack_print(&list_of_steps.configs[i].b);
        printf("\n");
        printf("C: ");
        stack_print(&list_of_steps.configs[i].c);
        printf("\n");
        printf("----------------------\n");
    }

    return 0;
}

bool __try_moving_the_top(Stack *src, Stack *dest) {
    if(stack_is_empty(src)) return false;
    int src_top = stack_pop(src);
    if(stack_is_empty(dest)) {
        stack_push(dest, src_top);
        return true;
    }
    int dest_top = stack_pop(dest);
    stack_push(dest, dest_top);
    if(dest_top < src_top) {
        return false;
    }
    stack_push(dest, src_top);
    return true;
}


ConfigurationList tower_of_hanoi  (Configuration initial, Configuration final, ConfigurationList *recurse_base_config_list) {
    /*
        printf("A: ");
        stack_print(&initial.a);
        printf("\n");
        printf("B: ");
        stack_print(&initial.b);
        printf("\n");
        printf("C: ");
        stack_print(&initial.c);
        printf("\n");
    */
    ConfigurationList base_config_list;
    if(recurse_base_config_list == NULL) {
        base_config_list = config_list_new();
    } else {
        base_config_list = *recurse_base_config_list;
        config_list_push_clone(&base_config_list, &initial);
    }

    ConfigurationList config_list_min;
    config_list_min.len = INT_MAX;

    ConfigurationList temp_result;
    ConfigurationList temp_base_list;
    Configuration temp_initial;

    if(configuration_equals(&initial, &final)) {
        return base_config_list;
    }

    // A -> B;
    temp_initial = configuration_clone(&initial);
    if(__try_moving_the_top(&temp_initial.a, &temp_initial.b) && !config_list_contains(&base_config_list, &temp_initial)) {
        temp_base_list = config_list_clone(&base_config_list);
        temp_result = tower_of_hanoi(temp_initial, final, &temp_base_list);
        if(temp_result.len < config_list_min.len) {
            config_list_min = temp_result;
        }
    }

    // A -> C;
    temp_initial = configuration_clone(&initial);
    if(__try_moving_the_top(&temp_initial.a, &temp_initial.c) && !config_list_contains(&base_config_list, &temp_initial)) {
        temp_base_list = config_list_clone(&base_config_list);
        temp_result = tower_of_hanoi(temp_initial, final, &temp_base_list);
        if(temp_result.len < config_list_min.len) {
            config_list_min = temp_result;
        }
    }

    // B -> C;
    temp_initial = configuration_clone(&initial);
    if(__try_moving_the_top(&temp_initial.b, &temp_initial.c)&& !config_list_contains(&base_config_list, &temp_initial)) {
        temp_base_list = config_list_clone(&base_config_list);
        temp_result = tower_of_hanoi(temp_initial, final, &temp_base_list);
        if(temp_result.len < config_list_min.len) {
            config_list_min = temp_result;
        }
    }

    // B -> A;
    temp_initial = configuration_clone(&initial);
    if(__try_moving_the_top(&temp_initial.b, &temp_initial.a)&& !config_list_contains(&base_config_list, &temp_initial)) {
        temp_base_list = config_list_clone(&base_config_list);
        temp_result = tower_of_hanoi(temp_initial, final, &temp_base_list);
        if(temp_result.len < config_list_min.len) {
            config_list_min = temp_result;
        }
    }

    // C -> A;
    temp_initial = configuration_clone(&initial);
    if(__try_moving_the_top(&temp_initial.c, &temp_initial.a)&& !config_list_contains(&base_config_list, &temp_initial)) {
        temp_base_list = config_list_clone(&base_config_list);
        temp_result = tower_of_hanoi(temp_initial, final, &temp_base_list);
        if(temp_result.len < config_list_min.len) {
            config_list_min = temp_result;
        }
    }

    // C -> B;
    temp_initial = configuration_clone(&initial);
    if(__try_moving_the_top(&temp_initial.c, &temp_initial.b)&& !config_list_contains(&base_config_list, &temp_initial)) {
        temp_base_list = config_list_clone(&base_config_list);
        temp_result = tower_of_hanoi(temp_initial, final, &temp_base_list);
        if(temp_result.len < config_list_min.len) {
            config_list_min = temp_result;
        }
    }
    return config_list_min;
}


/* Stack -------------------------------------------------------------------------- */

Stack stack_new(int capacity) {
    Stack stack;
    stack.data = malloc(sizeof(int)*capacity);
    stack.top = -1;
    stack.capacity = capacity;
    return stack;
}

void stack_push(Stack *self, int item) {
    assert(self->top < self->capacity - 1);
    self->top += 1;
    self->data[self->top] = item;
}

int stack_pop(Stack *self) {
    assert(self->top >= 0);
    int return_val = self->data[self->top];
    self->top -= 1;
    return return_val;
}

bool stack_is_empty(Stack *self) {
    return self->top < 0;
}

Stack stack_clone(Stack *self) {
    Stack s = stack_new(self->capacity);
    memcpy(s.data, self->data, self->capacity * sizeof(int));
    s.top = self->top;
    return s;
}

bool stack_equals   (Stack *self, Stack *other) {
    if(self->top != other->top) return false;
    for(int i=0; i<=self->top; i++) {
        if(self->data[i] != other->data[i]) return false;
    }
    return true;
}


void stack_print(Stack *self) {
    for(int i = 0; i <= self->top; i++) {
       printf("|%d  ", self->data[i]);
    }
}

/* Configuration List ------------------------------------------------------------- */

Configuration configuration_clone(Configuration *self) {
    Configuration c;
    c.a = stack_clone(&self->a);
    c.b = stack_clone(&self->b);
    c.c = stack_clone(&self->c);
    return c;
}

bool configuration_equals(Configuration *self, Configuration *other) {
    return stack_equals(&self->a, &other->a) &&
           stack_equals(&self->b, &other->b) &&
           stack_equals(&self->c, &other->c);
}

ConfigurationList config_list_new() {
    ConfigurationList config_list = {
        .capacity = 10,
        .configs = malloc(sizeof(Configuration) * 10),
        .len = 0
    };
    return config_list;
}

bool config_list_contains(ConfigurationList *self, Configuration *config) {
    for(int i=0; i<self->len; i++) {
        if(configuration_equals(&self->configs[i], config)) return true;
    }
    return false;
}

void config_list_push_clone(ConfigurationList *self, Configuration *config) {
    Configuration c = configuration_clone(config);
    if(self->len==self->capacity) {
        self->configs = realloc(self->configs, sizeof(Configuration) * self->capacity * 2);
        self->capacity *= 2;
        if(self->configs == NULL) {printf("OOM!!\n"); exit(1);}
    }
    self->configs[self->len] = c;
    self->len ++;
}

ConfigurationList config_list_clone(ConfigurationList *self) {
    ConfigurationList ret = {
        .configs = malloc(sizeof(Configuration) * self->capacity),
        .len = self->len,
        .capacity = self->capacity
    };
    memcpy(ret.configs, self->configs, sizeof(Configuration) * self->capacity);
    return ret;
}

/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
