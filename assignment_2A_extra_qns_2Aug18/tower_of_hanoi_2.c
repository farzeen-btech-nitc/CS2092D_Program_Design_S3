#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Tower of hanoi
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);


void transfer_top(int *src, int *dest);

typedef void (*callback)(void *);

void move_stack(int *src, int *spare, int *dest, callback cb_fn, void *cb_data) {
    if(*(src+1) == 0) {
        transfer_top(src, dest);
        cb_fn(cb_data);
    } else {
        move_stack(src+1,
                   dest, // dest is new spare
                   spare, // spare is new dest
                   cb_fn, cb_data
        );
        transfer_top(src, dest);
        cb_fn(cb_data);
        move_stack(spare, //src
                   src,   //spare
                   dest+1, //dest
                   cb_fn, cb_data
        );
    }
}

typedef struct {
    int *A, *B, *C;
    int no_of_callbacks;
} StateData ;

void state_update_cb(void *state_data);

int main() {

    printf("Enter N: ");
    int N;
    read_uint(&N);
    int src[N+1];
    int dest[N+1];
    int spare[N+1];

    src[N] = dest[N] = spare[N] = 0;
    for(int i = 0; i<N; i++) {
        src[i]   = N-i;
        dest[i]  = 0;
        spare[i] = 0;
    }

    StateData state_data = {src, spare, dest, 0};
    state_update_cb(&state_data);
    move_stack(src, spare, dest, state_update_cb, &state_data);
    printf("\nNo of steps: %d\n", state_data.no_of_callbacks - 1);
    return 0;
}

void transfer_top(int *src, int *dest) {
    *dest = *src;
    *src = 0;
}

void print_stack(int *stack) {
    while(*stack) {
        printf("%d ", *stack);
        stack++;
    }
}

void state_update_cb(void *state_data) {
    StateData *state = state_data;
    /*state->no_of_callbacks ++;
    static int last_len_A = 0,
               last_len_B = 0,
               last_len_C = 0;
    if(last_len_A + last_len_B + last_len_C > 0) {
        char src, dest;
        int val;
        if(state->A[last_len_A-1] == 0) {
            src = 'A';
       } else if(state->B[last_len_B-1] == 0) {
            src = 'B';
       } else if(state->C[last_len_C-1] == 0) {
            src = 'C';
       }
       if(state->A[last_len_A] != 0) {
            dest = 'A'; val = state->A[last_len_A];
       } else if(state->B[last_len_B] != 0) {
            dest = 'B'; val = state->B[last_len_B];
       } else if(state->C[last_len_C] != 0) {
            dest = 'C'; val = state->C[last_len_C];
       }
       printf("Move %d from %c to %c\n", val, src, dest);
    }

    for(last_len_A = 0; state->A[last_len_A] != 0;) last_len_A++;
    for(last_len_B = 0; state->B[last_len_B] != 0;) last_len_B++;
    for(last_len_C = 0; state->C[last_len_C] != 0;) last_len_C++;
    */
    printf("A: ");print_stack(state->A); printf("\n");
    printf("B: ");print_stack(state->B); printf("\n");
    printf("C: ");print_stack(state->C); printf("\n");
    printf("------------------------------------- \n");

}



/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("error: please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("error: please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("error: please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
