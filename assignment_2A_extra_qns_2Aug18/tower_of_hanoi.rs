use std::iter::FromIterator;
use std::io::{self, Write};

fn main() {
    print!("Enter N: "); io::stdout().flush().unwrap();
    let mut input = String::new();
    io::stdin().read_line(&mut input);

    let N = input.trim().parse::<u8>().unwrap();
    let state_initial = [
        Vec::from_iter((1..=N).rev()),
        Vec::new(),
        Vec::new()
    ];
    let state_final = [
        Vec::new(),
        Vec::new(),
        Vec::from_iter((1..=N).rev()),
    ];
    let list_of_states = tower_of_hanoi(&state_initial, &state_final, &Vec::new()).unwrap();
    println!(" No of steps: {}", list_of_states.len() - 1);
    for state in list_of_states {
        print_state(&state);
        println!("-----------------");
    }
}

type State = [Vec<u8>;3];

fn print_state(state: &State) {
    let stack_names = ["A", "B", "C"];
    for i in 0..3 {
        print!("{}| ", stack_names[i]);
        for i in &state[i] {
            print!("{} ", i);
        }
        print!("\n");
    }
}

fn state_equal(state1: &State, state2: &State) -> bool {
    for i in 0..3 {
        if state1[i].len() != state2[i].len() {
            return false;
        }

        for j in 0..state1[i].len() {
            if state1[i][j] != state2[i][j] {
                return false;
            }
        }
    }
    return true;
}

fn tower_of_hanoi(initial: &State, final_: &State, visited_states: &Vec<State>) -> Option<Vec<State>> {

    let mut visited_states = visited_states.clone();
    visited_states.push(initial.clone());
    //println!("Initial: ");
    //print_state(&initial);
    //println!("Final: ");
    //print_state(&final_);
    //println!("");
    if state_equal(&initial, &final_) {
        //println!("final");
        //print_state(&final_);
        return Some(visited_states);
    }

    let mut min_visited_states: Option<Vec<State>> = None;
    for i in [(0,1), (0,2), (1,0), (1,2), (2,0), (2,1)].iter() {
        let src_i = i.0;
        let dest_i = i.1;

        let mut initial_temp = initial.clone();
        if initial_temp[src_i].len() > 0 &&
           (  initial_temp[dest_i].len() == 0 ||
              initial[src_i].last().unwrap() < initial[dest_i].last().unwrap()) {

            let src = initial_temp[src_i].pop().unwrap();
            initial_temp[dest_i].push(src);

            {
            let contains = visited_states.iter().find(|&&ref x| state_equal(&x, &initial_temp));
                if contains.is_some() {
                    continue;
                }
            }
            let min_visited_states_candidate = tower_of_hanoi(&initial_temp, &final_, &visited_states);
            if min_visited_states_candidate.is_some() &&
               ( min_visited_states.is_none() ||
                 min_visited_states.as_ref().unwrap().len() > min_visited_states_candidate.as_ref().unwrap().len() ) {
                   min_visited_states = min_visited_states_candidate;
               }
        }
    }

    return min_visited_states;
}
