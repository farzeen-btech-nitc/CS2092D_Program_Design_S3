#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

/* Linked list functions ----------------------- */
struct LinkedList {
    int data;
    struct LinkedList *next;
};
typedef struct LinkedList *Node;

Node ll_create_node() {
   Node node = malloc(sizeof(struct LinkedList));
   node->next = NULL;
   return node;
}

int main() {


    return 0;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("Error: Please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("Error: Please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("Error: Please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
