#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

/**
 * Longest string of ones
 */

//#define DEBUG 1

void read_elements(int **arr, int *n);

struct OnesZeroOnes{
    int start_of_ones;
    int pos_single_zero;
    int end_of_ones;
};
typedef struct OnesZeroOnes OnesZeroOnes;

enum STATE {
    STATE_BEGIN_ONE_ENCOUNTERED = 10,
    STATE_MIDDLE_SINGLE_ZERO_ENCOUNTERED = 11,
    STATE_CONTINUING_ONE_ENCOUNTERED = 12,
    STATE_BEGIN_ONE_NOT_ENCOUNTERED = 13
};
typedef enum STATE STATE;

void copy_ones_zero_ones(OnesZeroOnes *src, OnesZeroOnes *dest) {
    dest->start_of_ones = src->start_of_ones;
    dest->pos_single_zero = src->pos_single_zero;
    dest->end_of_ones = src->end_of_ones;
}

int main() {
    int *array_of_bits;
    int len;

    read_elements(&array_of_bits, &len);

    // find 1-0-1's

    OnesZeroOnes ones_zero_ones[len*10]; // Why 10?
    int ones_zero_ones_len = 0;

    STATE cur_state = STATE_BEGIN_ONE_NOT_ENCOUNTERED;
    OnesZeroOnes temp_ones_zero_ones;
    temp_ones_zero_ones.start_of_ones = -1;
    temp_ones_zero_ones.end_of_ones = -1;
    temp_ones_zero_ones.pos_single_zero = -1;

    for(int i = 0; i<len; i++) {
        switch(cur_state) {
            case STATE_BEGIN_ONE_NOT_ENCOUNTERED:
                if(array_of_bits[i] == 1) {
                    temp_ones_zero_ones.start_of_ones = i;
                    cur_state = STATE_BEGIN_ONE_ENCOUNTERED;
                    continue;
                }
                break;
            case STATE_BEGIN_ONE_ENCOUNTERED:
                if(array_of_bits[i] == 0) {
                    temp_ones_zero_ones.pos_single_zero = i;
                    cur_state = STATE_MIDDLE_SINGLE_ZERO_ENCOUNTERED;
                    continue;
                }
                break;
            case STATE_MIDDLE_SINGLE_ZERO_ENCOUNTERED:
                if(array_of_bits[i] == 0) {
                    temp_ones_zero_ones.end_of_ones = -1;
                    copy_ones_zero_ones(&temp_ones_zero_ones, &ones_zero_ones[ones_zero_ones_len]);
                    ones_zero_ones_len ++;
                    cur_state = STATE_BEGIN_ONE_NOT_ENCOUNTERED;
                    continue;
                }
                if(array_of_bits[i] == 1) {
                    cur_state = STATE_CONTINUING_ONE_ENCOUNTERED;
                    temp_ones_zero_ones.end_of_ones = i;
                    continue;
                }
                break;
            case STATE_CONTINUING_ONE_ENCOUNTERED:
                if(array_of_bits[i] == 1) {
                    temp_ones_zero_ones.end_of_ones = i;
                    continue;
                }
                if(array_of_bits[i] == 0) {
                    copy_ones_zero_ones(&temp_ones_zero_ones, &ones_zero_ones[ones_zero_ones_len]);
                    #ifdef DEBUG
                    printf("DEBUG: Pushing to list: [%d] {%d, %d, %d} \n",ones_zero_ones_len,
                            temp_ones_zero_ones.start_of_ones,
                            temp_ones_zero_ones.pos_single_zero,
                            temp_ones_zero_ones.end_of_ones);
                    #endif
                    ones_zero_ones_len ++;
                    i = temp_ones_zero_ones.pos_single_zero+1;//start of continuing one
                    temp_ones_zero_ones.start_of_ones = i;
                    temp_ones_zero_ones.end_of_ones = -1;
                    temp_ones_zero_ones.pos_single_zero = -1;
                    cur_state = STATE_BEGIN_ONE_ENCOUNTERED;
                    continue;
                }
                break;
            default:
                printf("The programmer is an idiot.\n");
        }
    }
    #ifdef DEBUG
    printf("\n\n");
    #endif



    if(cur_state==STATE_CONTINUING_ONE_ENCOUNTERED) {
        ones_zero_ones[ones_zero_ones_len] = temp_ones_zero_ones;
        ones_zero_ones_len ++;
        cur_state = STATE_BEGIN_ONE_NOT_ENCOUNTERED;
    }
    if(cur_state==STATE_MIDDLE_SINGLE_ZERO_ENCOUNTERED) {
        temp_ones_zero_ones.end_of_ones = -1;
        copy_ones_zero_ones(&temp_ones_zero_ones, &ones_zero_ones[ones_zero_ones_len]);
        ones_zero_ones_len ++;
    }
    if(cur_state==STATE_BEGIN_ONE_ENCOUNTERED) {
        if(temp_ones_zero_ones.start_of_ones>0) {
            OnesZeroOnes new_temp;
            new_temp.start_of_ones = -1;
            new_temp.pos_single_zero = temp_ones_zero_ones.start_of_ones-1;
            new_temp.end_of_ones = len-1;
            ones_zero_ones[ones_zero_ones_len] = new_temp;
            ones_zero_ones_len++;
        }
    }

    int i_max = -1, val_max = 0;
    #ifdef DEBUG
    printf("DEBUG: OnesZeroOnes length: %d\n\n", ones_zero_ones_len);
    #endif
    for(int i = 0;i<ones_zero_ones_len; i++) {
        OnesZeroOnes curr_item = ones_zero_ones[i];
        #ifdef DEBUG
        printf("DEBUG: [%d] {%d, %d, %d} \n",i, curr_item.start_of_ones, curr_item.pos_single_zero, curr_item.end_of_ones);
        #endif

        int curr_len = 0;
        if(curr_item.end_of_ones == -1) {
            curr_len = curr_item.pos_single_zero - curr_item.start_of_ones; //excluding pos_zero, so no +1
        } else {
            curr_len = curr_item.end_of_ones - curr_item.start_of_ones + 1;
        }
        if(curr_len>val_max) {
            i_max = i;
            val_max = curr_len;
        }
    }
    if(ones_zero_ones_len>0) printf("Position %d\n", ones_zero_ones[i_max].pos_single_zero);
    else printf("There are no zeros to change. \n");
    return 0;
}

void read_elements(int **arr, int *n) {
    #ifdef QUICK_DEBUG
    static int arr1[13] = {1,1,0,0,1,0,1,1,1,0,1,1,1};
    *arr = arr1;
    *n = 13;
    #else
    printf("Number of elements in the array: ");
    errno = 0;
    if(!scanf("%d",n)||errno||*n<=0) {
        printf("ERROR: Invalid input.\n");
        exit(1);
    }
    int arr1[*n];
    printf("Elements: \n");
    *arr = arr1;
    for(int i=0;i<*n;i++) {
        printf("\t [%d]: ", i);
        if(!scanf("%d,", &arr1[i])||errno) {
            printf("ERROR: Invalid input.\n");
        }
    }
    #endif
}
