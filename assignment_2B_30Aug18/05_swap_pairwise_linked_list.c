#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Swap the elements of a linked list pairwise.
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

/* Linked list functions ----------------------- */
struct LinkedList {
    int data;
    struct LinkedList *next;
};
typedef struct LinkedList *Node;

Node ll_create_node() {
   Node node = malloc(sizeof(struct LinkedList));
   node->next = NULL;
   return node;
}

int main() {

    int N;
    printf("Enter the value of n\t\t\t: ");
    read_uint(&N);

    Node head = NULL;
    printf("Enter the elements of the linked list\t: ");
    if(N>0) {
        head = ll_create_node();
        read_int(&head->data);

        Node current = head;
        for(int i = 1; i<N; i++) {
            current->next = ll_create_node();
            current = current->next;
            read_int(&current->data);
        }
    }

    Node previous = NULL;
    Node current = head;
    head = head->next;
    while(current != NULL && current->next != NULL) {
        Node next = current->next;
        // connect previous to current->next
        if(previous != NULL) previous->next = next;
        // connect current to next->next
        current->next = next->next;
        // connect next to current
        next->next = current;
        // swap complete :)

        previous = current;
        current = current->next;
    }

    current = head;
    printf("Elements of the linked list\n"
           "after pairwise swapping\t\t\t: ");
    while(current != NULL) {
        printf("%d, ", current->data);
        current = current->next;
    }
    printf("\b\b ");
    printf("\n");

    return 0;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("Error: Please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("Error: Please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("Error: Please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
