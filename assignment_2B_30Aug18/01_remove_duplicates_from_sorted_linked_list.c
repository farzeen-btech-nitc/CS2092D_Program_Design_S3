#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Remove duplicates from sorted singly linked list of
 * integers
 */

/* utility functions --------------------------- */
void read_int   (int *var);
void read_uint  (int *var);
void read_nzint (int *var);

/* Linked list functions ----------------------- */
struct LinkedList {
    int data;
    struct LinkedList *next;
};
typedef struct LinkedList *Node;

Node ll_create_node() {
   Node node = malloc(sizeof(struct LinkedList));
   node->next = NULL;
   return node;
}

void ll_remove_next_node(Node current) {
    Node next      = current->next;
    current->next = current->next->next;
    free(next);
}

int main() {
    int N;
    printf("Enter the value of n:\t\t");
    read_uint(&N);

    printf("Enter the elements:\t\t");

    Node head = NULL;
    if(N>0) {
        head = ll_create_node();
        read_int(&(head->data));
    }

    Node current = head;
    for(int i = 1; i < N; i++) {
        current->next = ll_create_node();
        current = current->next;
        read_int(&(current->data));
    }

    current = head;
    while(current != NULL) {
        while(current->next != NULL &&
              current->data == current->next->data) {
            ll_remove_next_node(current);
        }
        current = current->next;
    }

    current = head;
    printf("The elements of the list:\t");
    while(current != NULL) {
        printf("%d, ", current->data);
        current = current->next;
    }
    printf("\b\b  \n");

    return 0;
}


/* impl utility functions ---------------------- */

void read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("Error: Please enter a valid integer.\n");
        exit(1);
    }
}

void read_uint  (int *var) {
    read_int(var);
    if(*var<0) {
        printf("Error: Please enter a valid positive integer.\n");
        exit(1);
    }
}

void read_nzint (int *var) {
    read_int(var);
    if(*var<=0) {
        printf("Error: Please enter a valid non-zero positive integer.\n");
        exit(1);
    }
}
