#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Employee Array of Structures
 */

/* utility functions --------------------------- */
int read_int   (int *var);
int read_uint  (int *var);

struct Employee {
   char name[100];
   int  salary;
   int  work_per_day_hours;
};
typedef struct Employee Employee;

struct Company {
    Employee employees[100];
    int      employees_len;
};
typedef struct Company Company;

void company_init(Company *company);

void add_employee(Company *company);
void display_employees_no_increment(Company *company);
void display_all_employees(Company *company);
void display_employees_given_work_per_day(Company *company);

int main() {

    Company company;
    company_init(&company);

    printf("Welcome\n\n");

    printf("Menu\n");
    printf("====\n");
    printf("\n");

    printf("(1) \t Add an employee record \n");
    printf("(2) \t Display the details of all the employess who did not get any increment in salary \n");
    printf("(3) \t Display the details of all the employees with their final salaries \n");
    printf("(4) \t Display the details of all the employees, given work per day (in hours) \n");
    printf("(5) \t Exit \n");

    while(1) {
        printf("\n");
        printf("Enter your choice\t: ");
        int choice;
        if(read_uint(&choice) != 0) continue;
        switch(choice) {
            case 1: add_employee(&company); break;
            case 2: display_employees_no_increment(&company); break;
            case 3: display_all_employees(&company); break;
            case 4: display_employees_given_work_per_day(&company); break;
            case 5: printf("\nExiting.\n"); return 0;
            default:
                    if(feof(stdin)) {
                        printf("\nExiting.\n"); return 0;
                    }
                    printf("Invalid input. Please try again.\n"); break;
        }
    }
    return 0;
}

void company_init(Company *company) {
    company->employees_len = 0;
}

void add_employee(Company *company) {
    Employee *employee = &company->employees[company->employees_len];
    printf("Name\t\t\t: ");
    getchar(); //to discard newlines
    for(int i = 0; 1 ; i++) {
        employee->name[i] = getchar();
        if(employee->name[i] == '\n') {
            employee->name[i] = '\0';
            break;
        }
    }

    printf("Salary\t\t\t: ");
    if(read_uint(&employee->salary) != 0) return;

    printf("Work per day (in hours)\t\t:");
    if(read_uint(&employee->work_per_day_hours) != 0) return;
    if(employee->work_per_day_hours >= 12) {
        employee->salary += 150;
    } else if(employee->work_per_day_hours >= 10) {
        employee->salary += 100;
    } else if(employee->work_per_day_hours >= 8) {
        employee->salary += 50;
    }

    company->employees_len += 1;
}

void display_employees_no_increment(Company *company) {
    printf("Name \t\t Salary \t Work per day (in hours) \n");
    for(int i=0; i<company->employees_len; i++) {
        Employee *rec = &company->employees[i];
        if(rec->work_per_day_hours < 8) {
            printf("%s \t %d \t\t %d \n", rec->name, rec->salary, rec->work_per_day_hours);
        }
    }
}

void display_all_employees(Company *company) {
    printf("Name \t\t Salary \t Work per day (in hours) \n");
    for(int i=0; i<company->employees_len; i++) {
        Employee *rec = &company->employees[i];
        printf("%s \t %d \t\t %d \n", rec->name, rec->salary, rec->work_per_day_hours);
    }
}

void display_employees_given_work_per_day(Company *company) {
    printf("Enter the work per day (in hours)\t: ");
    int work_per_day_hours;
    if(read_uint(&work_per_day_hours) != 0) return;
    printf("Name \t\t Salary \t Work per day (in hours) \n");
    for(int i=0; i<company->employees_len; i++) {
        Employee *rec = &company->employees[i];
        if(rec->work_per_day_hours == work_per_day_hours) {
            printf("%s \t %d \t\t %d \n", rec->name, rec->salary, rec->work_per_day_hours);
        }
    }
}

/* impl utility functions ---------------------- */

void clean_stdin() {
    while(getchar() != '\n');
}

int read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("Error: Please enter a valid integer.\n");
        clean_stdin();
        return 1;
    }
    return 0;
}

int read_uint  (int *var) {
    if(read_int(var) != 0) return 1;
    if(*var<0) {
        printf("Error: Please enter a valid positive integer.\n");
        clean_stdin();
        return 1;
    }
    return 0;
}
