#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef TEST
    int test_main();
    int main() {
        test_main();
    }
    #define main main_deactivated
#endif

/**
 * Program name:
 * Description goes here.
 */

/* utility functions --------------------------- */
int read_int   (int *var);
int read_uint  (int *var);

struct Record {
   char name[100];
   int  acc_no;
   int  balance;
};
typedef struct Record Record;

struct Bank {
    Record records[100];
    int    records_len;
};
typedef struct Bank Bank;

void bank_init(Bank *bank);

void add_customer_record(Bank *bank);
void display_customers_balance_less_than_200(Bank *bank);
void display_customers_balance_got_incremented(Bank *bank);
void display_all_customers(Bank *bank);

int main() {

    Bank bank;
    bank_init(&bank);

    printf("Welcome\n\n");

    printf("Menu\n");
    printf("====\n");
    printf("\n");

    printf("(1) \t Add a customer record \n");
    printf("(2) \t Display the names of customers having balance less than 200 \n");
    printf("(3) \t Display the details of customers whose balance amount got incremented \n");
    printf("(4) \t Display the details of all the customers \n");
    printf("(5) \t Exit \n");

    while(1) {
        printf("\n");
        printf("Enter your choice\t: ");
        int choice;
        if(read_uint(&choice) != 0) continue;
        switch(choice) {
            case 1: add_customer_record(&bank); break;
            case 2: display_customers_balance_less_than_200(&bank); break;
            case 3: display_customers_balance_got_incremented(&bank); break;
            case 4: display_all_customers(&bank); break;
            case 5: printf("\nExiting.\n"); return 0;
            default:
                    if(feof(stdin)) {
                        printf("\nExiting.\n");
                        return 0;
                    }
                    printf("Invalid input. Please try again.\n"); break;
        }
    }
    return 0;
}

void bank_init(Bank *bank) {
    bank->records_len = 0;
}

void add_customer_record(Bank *bank) {
    Record *record = &bank->records[bank->records_len];
    printf("Name\t\t\t: ");
    getchar(); //to discard newlines
    for(int i = 0; 1 ; i++) {
        record->name[i] = getchar();
        if(record->name[i] == '\n') {
            record->name[i] = '\0';
            break;
        }
    }
    printf("Account Number\t\t: ");
    if(read_uint(&record->acc_no) != 0) return;
    if(record->acc_no < 10000 || record->acc_no > 99999) {
        printf("Error: Account number should be 5 digits.\n");
        return;
    }
    printf("Balance\t\t\t:");
    if(read_uint(&record->balance) != 0) return;
    if(record->balance <= 10) {
        printf("Error: Balance should be more than 10.\n");
        return;
    }
    if(record->balance > 1000) {
        record->balance += 100;
    }

    bank->records_len += 1;
}

void display_customers_balance_less_than_200(Bank *bank) {
    for(int i=0;i<bank->records_len;i++) {
        if(bank->records[i].balance < 200) {
            printf("%s\n", bank->records[i].name);
        }
    }
}

void display_customers_balance_got_incremented(Bank *bank) {
    printf("Name\t\t Account Number\t\t Balance \n");
    for(int i=0; i<bank->records_len; i++) {
        Record *rec = &bank->records[i];
        if(rec->balance > 1100) {
            printf("%-16s %-24d  %-16d \n", rec->name, rec->acc_no, rec->balance);
        }
    }
}

void display_all_customers(Bank *bank) {
    printf("Name\t\t Account Number\t\t Balance \n");
    for(int i=0; i<bank->records_len; i++) {
        Record *rec = &bank->records[i];
        printf("%-16s %-24d  %-16d \n", rec->name, rec->acc_no, rec->balance);
    }
}

/* impl utility functions ---------------------- */

void clean_stdin() {
    while(getchar() != '\n');
}

int read_int   (int *var) {
    errno = 0;
    if(!scanf("%d,", var) || errno) {
        printf("Error: Please enter a valid integer.\n");
        clean_stdin();
        return 1;
    }
    return 0;
}

int read_uint  (int *var) {
    if(read_int(var) != 0) return 1;
    if(*var<0) {
        printf("Error: Please enter a valid positive integer.\n");
        clean_stdin();
        return 1;
    }
    return 0;
}
