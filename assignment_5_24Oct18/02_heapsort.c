#include <stdio.h>

void readfile(FILE *infile, int *array, int buf_len, int *len);
void heapsort(int *array, int len);
void writefile(FILE *outfile, int *array, int len);

int main(int argc, char **argv) {
    if (argc < 3) {
        printf("Syntax: heapsort INPUTFILE OUTPUTFILE\n");
        printf("\n");
        return 1;
    }

    char *infile_ = argv[1];
    char *outfile_ = argv[2];

    FILE *infile = fopen(infile_, "r");
    FILE *outfile = fopen(outfile_, "w");

    if (infile == NULL) {
        printf("Unable to open input file. \n");
        return 1;
    }

    if (outfile == NULL) {
        printf("Unable to open output file. \n");
        return 1;
    }

    int array[10000];
    int len = 0;
    readfile(infile, array, 10000, &len);
    heapsort(array, len);
    writefile(outfile, array, len);

    fclose(infile);
    fclose(outfile);
}

void readfile(FILE *infile, int *array, int buf_len, int *len) {
    fscanf(infile, "%d", len);
    if (*len > buf_len) {
        *len = 0;
        printf("file too large\n");
        return;
    }

    for (int i = 0; i < *len; i++) {
        fscanf(infile, "%d", &array[i]);
    }
}

void writefile(FILE *outfile, int *array, int len) {
    for (int i = 0; i < len; i++) {
        fprintf(outfile, "%d ", array[i]);
    }
}

void swap(int *array, int i, int j) {
    int temp = array[i];
    array[i] = array[j];
    array[j] = temp;
}

#define LEFT(i) (2 * (i) + 1)
#define RIGHT(i) (2 * (i) + 2)
#define PARENT(i) (((i)-1) / 2)

// fix the maxheap rooted at root, assuming the maxheaps rooted
// at its children are correct
void bubble_down(int *array, int root, int end) {
    int larger_child;
    if (RIGHT(root) <= end) {
        if (array[LEFT(root)] >= array[RIGHT(root)]) {
            larger_child = LEFT(root);
        } else {
            larger_child = RIGHT(root);
        }
    } else if (LEFT(root) <= end) {
        larger_child = LEFT(root);
    } else {
        return;
    }
    if (array[root] < array[larger_child]) {
        swap(array, root, larger_child);
        bubble_down(array, larger_child, end);
    }
}

void max_heapify(int *array, int len) {
    int start = PARENT(len - 1);
    while (start >= 0) {
        bubble_down(array, start, len - 1);
        start--;
    }
}

void heapsort(int *array, int len) {
    max_heapify(array, len);
    int end = len - 1;
    while (end > 0) {
        swap(array, end, 0);
        end--;
        bubble_down(array, 0, end);
    }
}
