#include <stdio.h>
#include <stdlib.h>

struct Node {
    int val;
    struct Node *left;
    struct Node *right;
    struct Node *parent;
};

typedef struct Node Node;

#define LEFT(i) (2 * (i) + 1)
#define RIGHT(i) (2 * (i) + 2)
#define PARENT(i) (((i)-1) / 2)

void array_to_binary_tree(Node *root, int *array, int start, int end) {
    if (start > end) {
        return;
    }
    root->val = array[start];
    if (LEFT(start) <= end) {
        root->left = malloc(sizeof(Node));
        root->left->val = array[LEFT(start)];
        root->left->parent = root;
        root->left->left = NULL;
        root->left->right = NULL;
    }
    if (RIGHT(start) <= end) {
        root->right = malloc(sizeof(Node));
        root->right->val = array[RIGHT(start)];
        root->right->parent = root;
        root->right->left = NULL;
        root->right->right = NULL;
    }
    array_to_binary_tree(root->left, array, LEFT(start), end);
    array_to_binary_tree(root->right, array, RIGHT(start), end);
}

void preorder(Node *head) {
    if (head == NULL)
        return;
    printf("%d ", head->val);
    preorder(head->left);
    preorder(head->right);
}

void postorder(Node *head) {
    if (head == NULL)
        return;
    postorder(head->left);
    postorder(head->right);
    printf("%d ", head->val);
}

void inorder(Node *head) {
    if (head == NULL)
        return;
    inorder(head->left);
    printf("%d ", head->val);
    inorder(head->right);
}

int main() {
    int N;
    printf("Enter the size of array: ");
    scanf("%d", &N);
    int array[N];
    printf("Elements: ");
    for (int i = 0; i < N; i++) {
        scanf("%d", &array[i]);
    }

    Node root = {0, 0, 0, 0};
    array_to_binary_tree(&root, array, 0, N - 1);

    printf("Preorder: ");
    preorder(&root);
    printf("\n");

    printf("Postorder: ");
    postorder(&root);
    printf("\n");

    printf("Inorder: ");
    inorder(&root);
    printf("\n");

    printf("\n");
}
