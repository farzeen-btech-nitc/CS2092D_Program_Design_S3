#include <stdio.h>
#include <stdlib.h>

struct StackNode {
    int val;
    struct StackNode *next;
};
typedef struct StackNode StackNode;

struct Stack {
    StackNode *top;
};
typedef struct Stack Stack;

void stack_init(Stack *stack);
void stack_push(Stack *stack);
void stack_pop(Stack *stack);
void stack_print(Stack *stack);

int main() {
    printf("Operations:\n"
           "  1. Push\n"
           "  2. Pop\n"
           "  3. Print\n"
           "  4. Exit\n");

    Stack stack;
    stack_init(&stack);

    int op;
    while (1) {
        printf("Enter the operation: ");
        if (scanf("%d", &op) != 1) {
            printf("Invalid input\n");
            return 1;
        }
        switch (op) {
        case 1:
            stack_push(&stack);
            break;
        case 2:
            stack_pop(&stack);
            break;
        case 3:
            stack_print(&stack);
            break;
        default:
            printf("Exiting .. \n");
            return 0;
        }
    }
}

void stack_init(Stack *stack) { stack->top = NULL; }

void stack_push(Stack *stack) {
    printf("Element: ");
    int e;
    scanf("%d", &e);
    StackNode *top_new = malloc(sizeof(StackNode));
    top_new->val = e;
    top_new->next = stack->top;
    stack->top = top_new;
}

void stack_pop(Stack *stack) {
    if (stack->top == NULL) {
        printf("UNDERFLOW\n");
        return;
    }

    StackNode *top_old = stack->top;
    printf("%d\n", top_old->val);
    stack->top = top_old->next;
    free(top_old);
}

void stack_print(Stack *stack) {
    if (stack->top == NULL) {
        printf("EMPTY\n");
        return;
    }
    StackNode *node = stack->top;
    while (node != NULL) {
        printf("%d ", node->val);
        node = node->next;
    }
    printf("\n");
}
