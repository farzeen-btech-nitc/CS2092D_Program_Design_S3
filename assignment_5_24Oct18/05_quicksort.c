#include <stdio.h>

void readfile(FILE *infile, int *array, int buf_len, int *len);
void quicksort(int *array, int start, int end);
void writefile(FILE *outfile, int *array, int len);

int main(int argc, char **argv) {
    if (argc < 3) {
        printf("Syntax: quicksort INPUTFILE OUTPUTFILE\n");
        printf("\n");
        return 1;
    }

    char *infile_ = argv[1];
    char *outfile_ = argv[2];

    FILE *infile = fopen(infile_, "r");
    FILE *outfile = fopen(outfile_, "w");

    if (infile == NULL) {
        printf("Unable to open input file. \n");
        return 1;
    }

    if (outfile == NULL) {
        printf("Unable to open output file. \n");
        return 1;
    }

    int array[10000];
    int len = 0;
    readfile(infile, array, 10000, &len);
    quicksort(array, 0, len - 1);
    writefile(outfile, array, len);

    fclose(infile);
    fclose(outfile);
}

void readfile(FILE *infile, int *array, int buf_len, int *len) {
    fscanf(infile, "%d", len);
    if (*len > buf_len) {
        *len = 0;
        printf("file too large\n");
        return;
    }

    for (int i = 0; i < *len; i++) {
        fscanf(infile, "%d", &array[i]);
    }
}

void writefile(FILE *outfile, int *array, int len) {
    for (int i = 0; i < len; i++) {
        fprintf(outfile, "%d ", array[i]);
    }
}

void swap(int *array, int i, int j) {
    int temp = array[i];
    array[i] = array[j];
    array[j] = temp;
}

int partition(int *A, int beg, int end) {
    int p = end;
    int i = beg, j = beg;
    while (j < end) {
        if (A[j] < A[p]) {
            swap(A, i, j);
            i++;
        }
        j++;
    }
    swap(A, p, i);
    return i;
}

void quicksort(int *array, int start, int end) {
    if (start >= end) {
        return;
    }
    int p = partition(array, start, end);
    quicksort(array, start, p - 1);
    quicksort(array, p + 1, end);
}
