#include <stdio.h>

struct Queue {
    int *array;
    int array_len;
    int head;
    int tail;
    int size;
};
typedef struct Queue Queue;

void queue_init(Queue *queue, int *array, int len);
void queue_enqueue(Queue *queue);
void queue_dequeue(Queue *queue);
void queue_print(Queue *queue);

int main() {
    printf("Operations:\n"
           "  1. Enqueue\n"
           "  2. Dequeue\n"
           "  3. Print\n"
           "  4. Exit\n");

    int N;
    printf("Enter the size of the array: ");
    scanf("%d", &N);
    int queue_array[N];

    Queue queue;
    queue_init(&queue, queue_array, N);

    int op;
    while (1) {
        printf("Enter the operation: ");
        if (scanf("%d", &op) != 1) {
            printf("Invalid input\n");
            return 1;
        }
        switch (op) {
        case 1:
            queue_enqueue(&queue);
            break;
        case 2:
            queue_dequeue(&queue);
            break;
        case 3:
            queue_print(&queue);
            break;
        default:
            printf("Exiting .. \n");
            return 0;
        }
    }
}

void queue_init(Queue *queue, int *array, int len) {
    queue->head = -1;
    queue->tail = 0;
    queue->size = 0;
    queue->array_len = len;
    queue->array = array;
}

void queue_enqueue(Queue *queue) {
    if (queue->size == queue->array_len) {
        printf("OVERFLOW\n");
        return;
    }
    printf("Element: ");
    int e;
    scanf("%d", &e);
    queue->head = queue->head + 1;
    if (queue->head == queue->array_len) {
        queue->head = 0;
    }
    queue->array[queue->head] = e;
    queue->size++;
}

void queue_dequeue(Queue *queue) {
    if (queue->size == 0) {
        printf("UNDERFLOW\n");
        return;
    }
    printf("%d\n", queue->array[queue->tail]);
    queue->tail = queue->tail + 1;
    if (queue->tail == queue->array_len) {
        queue->tail = 0;
    }
    queue->size--;
}

void queue_print(Queue *queue) {
    if (queue->size == 0) {
        printf("EMPTY\n");
        return;
    }
    for (int i = 0, j = queue->tail; i < queue->size; i++, j++) {
        if (j == queue->array_len)
            j = 0;
        printf("%d ", queue->array[j]);
    }
    printf("\n");
}
