#include <stdio.h>

struct Stack {
    int *array;
    int array_len;
    int top;
};
typedef struct Stack Stack;

void stack_init(Stack *stack, int *array, int len);
void stack_push(Stack *stack);
void stack_pop(Stack *stack);
void stack_print(Stack *stack);

int main() {
    printf("Operations:\n"
           "  1. Push\n"
           "  2. Pop\n"
           "  3. Print\n"
           "  4. Exit\n");

    int N;
    printf("Enter the size of the array: ");
    scanf("%d", &N);
    int stack_array[N];

    Stack stack;
    stack_init(&stack, stack_array, N);

    int op;
    while (1) {
        printf("Enter the operation: ");
        if (scanf("%d", &op) != 1) {
            printf("Invalid input\n");
            return 1;
        }
        switch (op) {
        case 1:
            stack_push(&stack);
            break;
        case 2:
            stack_pop(&stack);
            break;
        case 3:
            stack_print(&stack);
            break;
        default:
            printf("Exiting .. \n");
            return 0;
        }
    }
}

void stack_init(Stack *stack, int *array, int len) {
    stack->top = -1;
    stack->array_len = len;
    stack->array = array;
}

void stack_push(Stack *stack) {
    if (stack->top + 1 >= stack->array_len) {
        printf("OVERFLOW\n");
        return;
    }
    printf("Element: ");
    int e;
    scanf("%d", &e);
    stack->top += 1;
    stack->array[stack->top] = e;
}

void stack_pop(Stack *stack) {
    if (stack->top < 0) {
        printf("UNDERFLOW\n");
        return;
    }

    printf("%d\n", stack->array[stack->top]);
    stack->top--;
}

void stack_print(Stack *stack) {
    if (stack->top < 0) {
        printf("EMPTY\n");
        return;
    }
    for (int i = stack->top; i >= 0; i--) {
        printf("%d ", stack->array[i]);
    }
    printf("\n");
}
