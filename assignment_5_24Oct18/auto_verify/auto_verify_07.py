import sys
from subprocess import run
from random import uniform
from os import chdir

c_file = '../'+sys.argv[1]
infile = 'infile.txt'
outfile = 'outfile.txt'

run('mkdir -p tests'.split())
chdir('tests')

run(['gcc', '-g', '-o', 'test.out', c_file], check=True)

r = [int(uniform(0, 10000)) for i in range(0, 1000)]
with open(infile, 'w') as f:
    f.writelines(" ".join([str(x) for x in r]).strip())
r.sort()
run(['./test.out', 'infile.txt', 'outfile.txt'], check=True)
with open(outfile, 'r') as f:
    l = f.readline().strip()
    if l != " ".join([str(x) for x in r]).strip():
        print("Error:")
        print(" Expected: "+str(r))
        print(" Recieved: "+str(r))
    else:
        print("Test passed!")
