#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

struct Node {
    int i;
    int val;
    struct Node *next;
    struct Node *prev;
};
typedef struct Node Node;

/* NOTE: Head is a sentinel */

void readfile(FILE *infile, Node *head);
void heapsort(Node *head);
void writefile(FILE *outfile, Node *head);

void printheap(Node *head) {
    while (head->next != NULL) {
        head = head->next;
        printf("[%d] %d | ", head->i, head->val);
    }
    printf("\n");
}

int main(int argc, char **argv) {
    if (argc < 3) {
        printf("Syntax: heapsort INPUTFILE OUTPUTFILE\n");
        printf("\n");
        return 1;
    }

    char *infile_ = argv[1];
    char *outfile_ = argv[2];
    /* char *infile_ = "in.txt"; */
    /* char *outfile_ = "out.txt"; */

    FILE *infile = fopen(infile_, "r");
    FILE *outfile = fopen(outfile_, "w");

    if (infile == NULL) {
        printf("Unable to open input file. \n");
        return 1;
    }

    if (outfile == NULL) {
        printf("Unable to open output file. \n");
        return 1;
    }

    Node head;
    head.prev = NULL;
    head.next = NULL;
    head.i = -1;
    readfile(infile, &head);
    heapsort(&head);
    writefile(outfile, &head);

    fclose(infile);
    fclose(outfile);
}

void readfile(FILE *infile, Node *head) {
    int i = 0;
    Node *n = head;
    int val;
    while (1) {
        if (fscanf(infile, "%d", &val) != 1)
            break;
        n->next = malloc(sizeof(Node));
        n->next->prev = n;
        n = n->next;
        n->val = val;
        n->i = i;
        i++;
    }
}

void writefile(FILE *outfile, Node *head) {
    Node *n = head->next;
    while (n != NULL) {
        fprintf(outfile, "%d ", n->val);
        n = n->next;
    }
}

void swap(Node *a, Node *b) {
    int temp = a->val;
    a->val = b->val;
    b->val = temp;
}

Node *LEFT(Node *a) {
    Node *b = a;
    while (b != NULL && b->i != (2 * a->i) + 1) {
        b = b->next;
    }
    return b;
}

Node *RIGHT(Node *a) {
    Node *b = a;
    while (b != NULL && b->i != (2 * a->i) + 2) {
        b = b->next;
    }
    return b;
}

Node *PARENT(Node *a) {
    Node *b = a;
    while (b != NULL && b->i != (a->i - 1) / 2) {
        b = b->prev;
    }
    return b;
}

// fix the maxheap rooted at root, assuming the maxheaps rooted
// at its children are correct
void bubble_down(Node *root) {
    assert(root->prev != NULL);
    assert(root->i != -1);

    Node *larger_child;
    Node *left = LEFT(root);
    Node *right = RIGHT(root);
    if (right != NULL) {
        if (left->val >= right->val) {
            larger_child = left;
        } else {
            larger_child = right;
        }
    } else if (left != NULL) {
        larger_child = left;
    } else {
        return;
    }

    if (root->val < larger_child->val) {
        swap(root, larger_child);
        bubble_down(larger_child);
    }
}

void max_heapify(Node *head) {
    Node *end = head->next;
    if (end == NULL)
        return;
    while (end->next != NULL) {
        end = end->next;
    }
    Node *start = PARENT(end);
    while (start != head) {
        bubble_down(start);
        start = start->prev;
    }
}

void heapsort(Node *head) {
    max_heapify(head);
    Node *end = head->next;
    while (end->next != NULL) {
        end = end->next;
    }
    Node *real_end = end;
    while (end->prev->prev != NULL) {
        swap(head->next, end);
        end = end->prev;
        end->next = NULL;
        bubble_down(head->next);
    }
    while (real_end->prev != NULL) {
        real_end->prev->next = real_end;
        real_end = real_end->prev;
    }
}
